//test script variables identified by test_
bool test_ready = false;

const uint8_t bmpTestSprite[] = {
  0b11111111, 0b11111111,  // Row 1
  0b10000010, 0b10101011,  // Row 2
  0b11010101, 0b01010101,  // Row 3
  0b11111111, 0b11111111,  // Row 4
};


void testLoopA() {
  test_ready = false; //Set all variables initial state here, since we can't guarantee this is the first run. 
  display.clearDisplay();
  delay(250); //Give loop B a moment to be sure it's ready

  //Be sure the program variables and state are all set before starting loop B. 
  test_ready = true; //used to let the other loop know it's safe to run. 
  
  while (mode == "LIGHTSOUT") {
    display.clearDisplay(); //Keep display, neopixel on one loop to avoid collisions. 
    display.setCursor(0, 0);
    display.print("Test!");
  }
}


//todo: Use macropad code for input in loop A
void testLoopB() {
  while (mode == "TEST") {
    if (test_ready) {
      delay(3);
      mode = "MAIN";// setting mode to MAIN releases control to the main program.
    }
  }
}