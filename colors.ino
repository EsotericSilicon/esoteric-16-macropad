
// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t RainbowWheel(byte WheelPos) {
  if (WheelPos < 85) {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
    WheelPos -= 170;
    return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}

uint32_t BlueGreenWheel(byte WheelPos) {
  if (WheelPos < 85) {
    return pixels.Color(0, WheelPos * 3, 255);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, 255, 255 - WheelPos * 3);
  } else {
    WheelPos -= 170;
    return pixels.Color(0, 255 - WheelPos * 3, 255 - WheelPos * 3);
  }
}

uint32_t ColorWheel(byte WheelPos, byte color1, byte color2, byte lum1, byte lum2) {
  // Map WheelPos from 0-255 to color1-color2
  byte colorPos = map(WheelPos, 0, 255, color1, color2);

  // Calculate the RGB components
  byte r, g, b;
  if (colorPos < 85) {
    r = 255 - colorPos * 3;
    g = colorPos * 3;
    b = 0;
  } else if (colorPos < 170) {
    colorPos -= 85;
    r = 0;
    g = 255 - colorPos * 3;
    b = colorPos * 3;
  } else {
    colorPos -= 170;
    r = colorPos * 3;
    g = 0;
    b = 255 - colorPos * 3;
  }

  // Map WheelPos from 0-255 to lum1-lum2
  byte lum = map(WheelPos, 0, 255, lum1, lum2);

  // Apply the luminance factor
  r = r * lum / 255;
  g = g * lum / 255;
  b = b * lum / 255;

  return pixels.Color(r, g, b);
}