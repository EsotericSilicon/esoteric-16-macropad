

void drawExpandingRectangles() {
  // New window position and size
  int windowX = display.width() * 0.05;
  int windowY = 12;
  int windowW = display.width() * 0.9;
  int windowH = display.height() * 4 / 5;

  // Start the animation from the center of the new window position
  int x = windowX + windowW / 2;
  int y = windowY + windowH / 2;
  int w = 0;
  int h = 0;

  while (w < windowW && h < windowH) {
    // Draw a white rectangle
    display.drawRect(x - w / 2, y - h / 2, w, h, SH110X_WHITE);
    display.display();
    delay(50);  // Adjust as needed

    // Draw a black rectangle over the white one
    display.drawRect(x - w / 2, y - h / 2, w, h, SH110X_BLACK);

    // Increase the size of the rectangle
    w += 16;  // Adjust as needed
    h += 8;   // Adjust as needed
  }
}


void randomPixelScreensaver() {
  // Clear the display
  display.clearDisplay();

  // Choose a random pixel
  int x = random(display.width() - 1);
  int y = random(display.height() - 1);

  // Set the pixel to black or white
  display.drawPixel(x, y, random(2) ? SH110X_BLACK : SH110X_WHITE);

  // Update the display every 10 pixels
  static int pixelCount = 0;
  pixelCount++;
  if (pixelCount % 10 == 0) {
    display.display();
  }
  delay(100);
}




void fillScreenRandomlyPixels() {
  // Create an array to hold the coordinates of each pixel
  int pixelCoords[display.width() * display.height()][2];
  int pixelCount = 0;

  // Fill the array with the coordinates of each pixel
  for (int x = 0; x < display.width(); x++) {
    for (int y = 0; y < display.height(); y++) {
      pixelCoords[pixelCount][0] = x;
      pixelCoords[pixelCount][1] = y;
      pixelCount++;
    }
  }

  // Randomly swap each pixel's coordinates with another pixel's coordinates
  for (int i = 0; i < pixelCount; i++) {
    int j = random(pixelCount);
    int tempX = pixelCoords[i][0];
    int tempY = pixelCoords[i][1];
    pixelCoords[i][0] = pixelCoords[j][0];
    pixelCoords[i][1] = pixelCoords[j][1];
    pixelCoords[j][0] = tempX;
    pixelCoords[j][1] = tempY;
  }

  // Draw each pixel in the random order
  for (int i = 0; i < pixelCount; i++) {
    display.drawPixel(pixelCoords[i][0], pixelCoords[i][1], SH110X_BLACK);
    display.display();
  }
}

void fillScreenRandomlyVertical() {
  fillScreenRandomlyVertical(90, 3);
}

void fillScreenRandomlyVertical(int amt, int theDelay) {
  // Create an array to hold the x-coordinates of each column.
  // amt = number of columns to fill. End early with less than your display's width.
  // delay = ms delay between frames. Increase if appearing all at once, decrease if too slow.
  int columnCoords[display.width()];
  int columnCount = display.width();

  // Fill the array with the x-coordinates of each column
  for (int x = 0; x < display.width(); x++) {
    columnCoords[x] = x;
  }

  // Randomly swap each column's x-coordinate with another column's x-coordinate
  for (int i = 0; i < columnCount; i++) {
    int j = random(columnCount);
    int tempX = columnCoords[i];
    columnCoords[i] = columnCoords[j];
    columnCoords[j] = tempX;
  }

  // Draw each column in the random order
  for (int i = 0; i < amt; i++) {
    for (int y = 0; y < display.height(); y++) {
      display.drawPixel(columnCoords[i], y, SH110X_BLACK);
    }
    delay(theDelay);
    display.display();
  }
}

void fillScreenRandomlyHorizontal(int amt, int theDelay) {
  // Create an array to hold the x-coordinates of each column.
  // amt = number of columns to fill. End early with less than your display's width.
  // delay = ms delay between frames. Increase if appearing all at once, decrease if too slow.
  int rowCoords[display.height()];
  int rowCount = display.height();

  // Fill the array with the x-coordinates of each row
  for (int x = 0; x < display.height(); x++) {
    rowCoords[x] = x;
  }

  // Randomly swap each row's x-coordinate with another row's x-coordinate
  for (int i = 0; i < rowCount; i++) {
    int j = random(rowCount);
    int tempY = rowCoords[i];
    rowCoords[i] = rowCoords[j];
    rowCoords[j] = tempY;
  }

  // Draw each row in the random order
  for (int i = 0; i < amt; i++) {
    for (int x = 0; x < display.height(); x++) {
      display.drawPixel(x, rowCoords[i], SH110X_BLACK);
    }
    delay(theDelay);
    display.display();
  }
}

void fillScreenRandomlyHV(int amt, int theDelay) {
  // Create an array to hold the x-coordinates of each column.
  // amt = number of columns to fill. End early with less than your display's width.
  // delay = ms delay between frames. Increase if appearing all at once, decrease if too slow.
  int columnCoords[display.width()];
  int columnCount = display.width();

  // Fill the array with the x-coordinates of each column
  for (int x = 0; x < display.width(); x++) {
    columnCoords[x] = x;
  }

  // Randomly swap each column's x-coordinate with another column's x-coordinate
  for (int i = 0; i < columnCount; i++) {
    int j = random(columnCount);
    int tempX = columnCoords[i];
    columnCoords[i] = columnCoords[j];
    columnCoords[j] = tempX;
  }

  // Draw each column in the random order
  for (int i = 0; i < amt; i++) {
    for (int y = 0; y < display.height(); y++) {
      display.drawPixel(columnCoords[i], y, SH110X_BLACK);
    }
    delay(theDelay);
    display.display();
  }
}


// Function to draw a bitmap from RAM
void drawBitmapFromRAM(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, bool reverseBits = true) {
  for (int16_t j = 0; j < h; j++, y++) {
    for (int16_t i = 0; i < w; i++) {
      uint8_t byte = bitmap[j * ((w + 7) / 8) + i / 8];
      uint8_t bit;
      if (reverseBits) {
        bit = byte & (1 << (i & 7));  // Reverse the bits
      } else {
        bit = byte & (128 >> (i & 7));
      }
      if (bit) {
        display.drawPixel(x + i, y, color);
      }
    }
  }
}

// Function to draw a pixelated bitmap from RAM
void drawPixelatedBitmapFromRAM(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, int pixelSize, bool reverseBits = true) {
  for (int16_t j = 0; j < h; j += pixelSize, y += pixelSize) {
    for (int16_t i = 0; i < w; i += pixelSize) {
      bool pixel = false;
      // Average the pixels in the current block
      for (int16_t pj = 0; pj < pixelSize; pj++) {
        for (int16_t pi = 0; pi < pixelSize; pi++) {
          uint8_t byte = bitmap[(j + pj) * ((w + 7) / 8) + (i + pi) / 8];
          uint8_t bit;
          if (reverseBits) {
            bit = byte & (1 << ((i + pi) & 7));  // Reverse the bits
          } else {
            bit = byte & (128 >> ((i + pi) & 7));
          }
          if (bit) {
            pixel = true;
            break;
          }
        }
        if (pixel) break;
      }
      if (pixel) {
        display.fillRect(x + i, y, pixelSize, pixelSize, color);
      }
    }
  }
}

// Function to draw a scaled bitmap from RAM
void drawScaledBitmapFromRAM(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, float scale, bool reverseBits = true) {
  for (int16_t j = 0; j < h * scale; j++) {
    for (int16_t i = 0; i < w * scale; i++) {
      uint8_t byte = bitmap[int(j / scale) * ((w + 7) / 8) + int(i / scale) / 8];
      uint8_t bit;
      if (reverseBits) {
        bit = byte & (1 << (int(i / scale) & 7));  // Reverse the bits
      } else {
        bit = byte & (128 >> (int(i / scale) & 7));
      }
      if (bit) {
        display.drawPixel(x + i, y + j, color);
      }
    }
  }
}

// Function to draw a swirled bitmap from RAM
void drawSwirledBitmapFromRAM(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, float angle, bool reverseBits = true) {
  float cx = w / 2.0;
  float cy = h / 2.0;
  for (int16_t j = 0; j < h; j++) {
    for (int16_t i = 0; i < w; i++) {
      // Calculate the distance and angle to the center of the image
      float dx = i - cx;
      float dy = j - cy;
      float dist = sqrt(dx * dx + dy * dy);
      float theta = atan2(dy, dx);

      // Add the swirl angle
      theta += dist / w * angle;

      // Convert the polar coordinates back to Cartesian coordinates
      int16_t xi = cx + dist * cos(theta);
      int16_t yi = cy + dist * sin(theta);

      // Draw the pixel if it's within the bounds of the image
      if (xi >= 0 && xi < w && yi >= 0 && yi < h) {
        uint8_t byte = bitmap[yi * ((w + 7) / 8) + xi / 8];
        uint8_t bit;
        if (reverseBits) {
          bit = byte & (1 << (xi & 7));  // Reverse the bits
        } else {
          bit = byte & (128 >> (xi & 7));
        }
        if (bit) {
          // Randomly skip rendering white pixels based on the current angle
          float skipChance = angle / (1.5 * PI);
          if (random(100) / 100.0 >= skipChance) {
            display.drawPixel(x + i, y + j, color);
          }
        }
      }
    }
  }
}

// // Function to draw a swirled and scaled bitmap from RAM
// void drawSwirledScaledBitmapFromRAM(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, float angle, float scale, bool reverseBits = true) {
//   float cx = w / 2.0;
//   float cy = h / 2.0;
//   for(int16_t j=0; j<h*scale; j+=2) {
//     for(int16_t i=0; i<w*scale; i+=2) {
//       // Calculate the distance and angle to the center of the image
//       float dx = i/scale - cx;
//       float dy = j/scale - cy;
//       float dist = sqrt(dx * dx + dy * dy);
//       float theta = atan2(dy, dx);

//       // Add the swirl angle
//       theta += dist / w * angle;

//       // Convert the polar coordinates back to Cartesian coordinates
//       int16_t xi = cx + dist * cos(theta);
//       int16_t yi = cy + dist * sin(theta);

//       // Draw the pixel if it's within the bounds of the image
//       if(xi >= 0 && xi < w && yi >= 0 && yi < h) {
//         uint8_t byte = bitmap[yi * ((w + 7) / 8) + xi / 8];
//         uint8_t bit;
//         if (reverseBits) {
//           bit = byte & (1 << (xi & 7));  // Reverse the bits
//         } else {
//           bit = byte & (128 >> (xi & 7));
//         }
//         if (bit) {
//           // Draw a 2x2 block of pixels instead of a single pixel
//           display.fillRect(x + i - (w * scale - w) / 2, y + j - (h * scale - h) / 2, 2, 2, color);
//         }
//       }
//     }
//   }
// }

// Function to slide the framebuffer out to the right
void slide_out() {
  uint8_t snapshot[64 * 128 / 8];

  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));


  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }

  for (int offset = 0; offset < 128; offset = offset + 4) {
    // Clear the display
    display.clearDisplay();

    // Draw the snapshot, shifted to the right
    drawBitmapFromRAM(offset, 0, snapshot, 128, 64, SH110X_WHITE);

    // Update the display
    display.display();

    // Wait for a short delay
    delay(5);  // Adjust as needed
  }
}

// Function to slide the framebuffer in from the left
void slide_in() {
  uint8_t snapshot[64 * 128 / 8];

  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));

  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }
  display.clearDisplay();

  display.setContrast(255);

  for (int offset = 128; offset >= 0; offset = offset - 4) {
    // Clear the display
    display.clearDisplay();

    // Draw the snapshot, shifted to the left
    drawBitmapFromRAM(-offset, 0, snapshot, 128, 64, SH110X_WHITE);

    // Update the display
    display.display();

    // Wait for a short delay
    delay(5);  // Adjust as needed
  }
}

// Function to slide the framebuffer in with pixelation effect
void pixelate_out() {
  uint8_t snapshot[64 * 128 / 8];


  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));

  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }

  for (int pixelSize = 1; pixelSize <= 64; pixelSize *= 2) {
    // Clear the display
    display.clearDisplay();

    // Draw the snapshot with pixelation
    drawPixelatedBitmapFromRAM(0, 0, snapshot, 128, 64, SH110X_WHITE, pixelSize);

    // Update the display
    display.display();

    // Wait for a short delay
    delay(32);  // Adjust as needed
  }
}


// Function to shrink the framebuffer out to the center
void shrink_out() {
  uint8_t snapshot[64 * 128 / 8];


  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));

  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }

  for (float scale = 1.0; scale >= 0.1; scale -= 0.05) {
    // Clear the display
    display.clearDisplay();

    // Draw the snapshot, scaled down
    drawScaledBitmapFromRAM(64 - 64 * scale, 32 - 32 * scale, snapshot, 128, 64, SH110X_WHITE, scale);

    // Update the display
    display.display();

    // Wait for a short delay
    delay(10);  // Adjust as needed
  }
}

// Function to grow the framebuffer in from the center
void grow_in() {
  uint8_t snapshot[64 * 128 / 8];


  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));

  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }

  for (float scale = 0.1; scale <= 1.0; scale += 0.05) {
    // Clear the display
    display.clearDisplay();

    // Draw the snapshot, scaled up
    drawScaledBitmapFromRAM(64 - 64 * scale, 32 - 32 * scale, snapshot, 128, 64, SH110X_WHITE, scale);

    // Update the display
    display.display();

    // Wait for a short delay
    delay(10);  // Adjust as needed
  }
}



// Function to swirl the framebuffer
void swirl_out() {
  uint8_t snapshot[64 * 128 / 8];

  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));

  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }

  for (float angle = 0; angle <= 1.5 * PI; angle += PI / 5) {
    // Clear the display
    display.clearDisplay();

    // Draw the snapshot with a swirl effect
    drawSwirledBitmapFromRAM(0, 0, snapshot, 128, 64, SH110X_WHITE, angle);

    // Update the display
    display.display();

    // Wait for a short delay
    //delay(10);  // Adjust as needed
  }
}

// Function to draw a swirled and scaled bitmap from RAM
void drawSwirledScaledBitmapFromRAM(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, float angle, float scale, bool reverseBits = true) {
  float cx = w / 2.0;
  float cy = h / 2.0;
  for (int16_t j = 0; j < h * scale; j += 2) {
    for (int16_t i = 0; i < w * scale; i += 2) {
      // Calculate the distance and angle to the center of the image
      float dx = i / scale - cx;
      float dy = j / scale - cy;
      float dist = sqrt(dx * dx + dy * dy);
      float theta = atan2(dy, dx);

      // Add the swirl angle
      theta += dist / w * angle;

      // Convert the polar coordinates back to Cartesian coordinates
      int16_t xi = cx + dist * cos(theta);
      int16_t yi = cy + dist * sin(theta);

      // Draw the pixel if it's within the bounds of the image
      if (xi >= 0 && xi < w && yi >= 0 && yi < h) {
        uint8_t byte = bitmap[yi * ((w + 7) / 8) + xi / 8];
        uint8_t bit;
        if (reverseBits) {
          bit = byte & (1 << (xi & 7));  // Reverse the bits
        } else {
          bit = byte & (128 >> (xi & 7));
        }
        if (bit) {
          // Draw a 2x2 block of pixels instead of a single pixel
          display.fillRect(x + i - (w * scale - w) / 2, y + j - (h * scale - h) / 2, 2, 2, color);
        }
      }
    }
  }
}

// // Function to draw a swirled and scaled bitmap from RAM
// void drawSwirledScaledBitmapFromRAMOld(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h, uint16_t color, float angle, float scale, bool reverseBits = true) {
//   float cx = w / 2.0;
//   float cy = h / 2.0;
//   for (int16_t j = 0; j < h * scale; j++) {
//     for (int16_t i = 0; i < w * scale; i++) {
//       // Calculate the distance and angle to the center of the image
//       float dx = i / scale - cx;
//       float dy = j / scale - cy;
//       float dist = sqrt(dx * dx + dy * dy);
//       float theta = atan2(dy, dx);

//       // Add the swirl angle
//       theta += dist / w * angle;

//       // Convert the polar coordinates back to Cartesian coordinates
//       int16_t xi = cx + dist * cos(theta);
//       int16_t yi = cy + dist * sin(theta);

//       // Draw the pixel if it's within the bounds of the image
//       if (xi >= 0 && xi < w && yi >= 0 && yi < h) {
//         uint8_t byte = bitmap[yi * ((w + 7) / 8) + xi / 8];
//         uint8_t bit;
//         if (reverseBits) {
//           bit = byte & (1 << (xi & 7));  // Reverse the bits
//         } else {
//           bit = byte & (128 >> (xi & 7));
//         }
//         if (bit) {
//           // Randomly skip rendering white pixels based on the current angle
//           //float skipChance = angle / (2 * PI);
//           //if (random(100) / 100.0 >= skipChance) {
//           display.drawPixel(x + i - (w * scale - w) / 2, y + j - (h * scale - h) / 2, color);
//           //}
//         }
//       }
//     }
//   }
// }

// Function to swirl the framebuffer
void swirl_scale_out() {
  uint8_t snapshot[64 * 128 / 8];

  // Clear the snapshot array
  memset(snapshot, 0, sizeof(snapshot));

  // Save a snapshot of the framebuffer
  for (int y = 0; y < 64; y++) {
    for (int x = 0; x < 128; x++) {
      if (display.getPixel(x, y)) {
        snapshot[y * 16 + x / 8] |= 1 << (x % 8);
      }
    }
  }

  for (float angle = 0; angle <= 3 * PI; angle += PI / 3) {
    // Clear the display
    display.clearDisplay();

    // Calculate the scale based on the current angle
    float scale = 1.0 + angle / (1.5 * PI);

    // Draw the snapshot with a swirl and scale effect
    drawSwirledScaledBitmapFromRAM(0, 0, snapshot, 128, 64, SH110X_WHITE, angle, scale);

    // Update the display
    display.display();

    // Wait for a short delay
    //delay(10);  // Adjust as needed
  }
}

void transitionIn() {
  if (tranIn == "slide") {
    slide_in();
    drawPage();
  } else if (tranIn == "zoom") {
    grow_in();
    drawPage();
  }
}

void transitionOut() {
  if (tranOut == "slide") {
    slide_out();
  } else if (tranOut == "pixel") {
    pixelate_out();
  } else if (tranOut == "swirl") {
    swirl_out();
  } else if (tranOut == "swirlZ") {
    swirl_scale_out();
  } else if (tranOut == "zoom") {
    shrink_out();
  } else if (tranOut == "Vlines") {
    fillScreenRandomlyVertical();
  }else if (tranOut == "Hlines") {
    fillScreenRandomlyVertical();
  }else if (tranOut == "Vlines") {
    fillScreenRandomlyVertical();
  }else if (tranOut == "HVlines") {
    fillScreenRandomlyVertical();
  }else if (tranOut == "Vlines") {
    fillScreenRandomlyVertical();
  }
}
