
const uint8_t bmpBreakoutBlockA[] = {
  0b11111111, 0b11111111,  // Row 1
  0b11000111, 0b11111111,  // Row 2
  0b11111111, 0b11111111,  // Row 3
  0b11111111, 0b11111111,  // Row 4
};

const uint8_t bmpBreakoutBlockB[] = {
  0b11111111, 0b11111111,  // Row 1
  0b10000010, 0b10101011,  // Row 2
  0b11010101, 0b01010101,  // Row 3
  0b11111111, 0b11111111,  // Row 4
};

const uint8_t bmpBreakoutBlockC[] = {
  0b11111111, 0b11111111,  // Row 1
  0b10000000, 0b00000001,  // Row 2
  0b10000000, 0b00000001,  // Row 3
  0b11111111, 0b11111111,  // Row 4
};
const uint8_t bmpBreakoutPaddle[] = {
  0b11111111, 0b11111111,  // Row 1
  0b10111000, 0b00000001,  // Row 2
  0b10000000, 0b00000001,  // Row 3
  0b11111111, 0b11111111,  // Row 4
};

const uint8_t bmpBreakoutPaddle2[] = {
  0b11111111, 0b11111111, 0b11111111, 0b11111111,  // Row 1
  0b10111000, 0b00000000, 0b00000000, 0b00000001,  // Row 2
  0b10000000, 0b00000000, 0b00000000, 0b00000001,  // Row 3
  0b11111111, 0b11111111, 0b11111111, 0b11111111,  // Row 4
};
const uint8_t bmpBreakoutPaddle2F[] = {
  0b10101010, 0b10101010, 0b10101010, 0b10101010,  // Row 1
  0b00111000, 0b00000000, 0b00000000, 0b00000001,  // Row 2
  0b10000000, 0b00000000, 0b00000000, 0b00000000,  // Row 3
  0b01010101, 0b01010101, 0b01010101, 0b01010101,  // Row 4
};

const uint8_t bmpBreakoutBall[] = {
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b00111100,  // Row 8
};
const uint8_t bmpBreakoutLife[] = {
  0b01100000,  // Row 1
  0b10110000,  // Row 2
  0b11110000,  // Row 3
  0b01100000,  // Row 4
};
const uint8_t bmpBreakoutPower[] = {
  0b11110000,  // Row 1
  0b10110000,  // Row 2
  0b11110000,  // Row 3
  0b11110000,  // Row 4
};

struct Block {
  int x;
  int y;
  int health;
  const uint8_t* bitmap;
};
struct PowerUp {
  int x;
  int y;
  int vy;  // Y velocity
  const uint8_t* bitmap;
};
#define BLOCK_WIDTH 16
#define BLOCK_HEIGHT 4
#define NUM_BLOCKS_X 8  // Number of blocks in a row
#define NUM_BLOCKS_Y 3  // Number of rows of blocks
#define MAX_BLOCKS_Y 7
#define NUM_LIVES 3
#define MAX_POWERUPS 5

unsigned long powerupTimer = 0;  // Timer for the power-up, in milliseconds
bool paddleFlashing = false;     // Whether the paddle is currently flashing

PowerUp powerups[MAX_POWERUPS];
int numPowerups = 0;
int paddleWidth = 16;
int lives = NUM_LIVES;

Block blocks[MAX_BLOCKS_Y][NUM_BLOCKS_X];



int level = 1;  // Start at level 1

// Ball position and velocity
int ballX = 60;
int ballY = 49;
int ballVX = -2;
int ballVY = -2;


// Paddle position
int paddleX = 56;

//128x64 display

int bk_oldPos = encoder.getPosition();
int bk_newPos = bk_oldPos;

bool firstFrame = true;
bool newFrame = false;
bool bk_ready = false;
bool paused = false;  // Whether the game is paused

unsigned long lastFlashTime = 0;  // The last time the paddle flashed


void breakoutLoopA() {
  // Ball position and velocity
  ballX = 60;
  ballY = 49;
  ballVX = -2;
  ballVY = -2;
  paddleX = 56;
  lives = NUM_LIVES;
  firstFrame = true;
  level = 3;
  newFrame = false;
  paddleWidth = 16;
  powerupTimer = 0;
  paused = false;

  paddleFlashing = false;
  for (int i = 0; i < MAX_BLOCKS_Y; i++) {
    for (int j = 0; j < NUM_BLOCKS_X; j++) {
      blocks[i][j].health = 0;
    }
  }

  // Reset power-ups
  numPowerups = 0;
  // Initialize blocks
  for (int i = 0; i < MAX_BLOCKS_Y; i++) {
    for (int j = 0; j < NUM_BLOCKS_X; j++) {
      blocks[i][j].x = j * BLOCK_WIDTH;
      blocks[i][j].y = i * BLOCK_HEIGHT;
      // Only initialize blocks for the current level, or all blocks for level 4
      blocks[i][j].health = (i < level || level == 4) ? 3 : 0;
      blocks[i][j].bitmap = bmpBreakoutBlockA;  // Start with bitmap A
    }
  }

  bk_ready = true;  //Allows second core to start


  while (mode == "BREAKOUT") {
    // Check if key 0 is pressed
    if (!digitalRead(1)) {
      paused = !paused;  // Toggle paused
      delay(200);        // Debounce delay
    }
    if (!paused) {
      encoder.tick();
      int bk_newPos = encoder.getPosition();
      int diff = abs(bk_newPos - bk_oldPos);  // Calculate the difference

      if (diff > 0) {                     // Check if the position has changed
        for (int i = 0; i < diff; i++) {  // Loop for the difference
          if (bk_newPos < bk_oldPos) {
            paddleX = paddleX + 2;
            if (paddleX > 128 - paddleWidth) {
              paddleX = 128 - paddleWidth;
            }
          } else {
            paddleX = paddleX - 2;
            if (paddleX < 0) {
              paddleX = 0;
            }
          }
        }
        bk_oldPos = bk_newPos;
      }
    } else {
      bk_oldPos = encoder.getPosition();
    }
  }
}


void breakoutLoopB() {
  while (mode == "BREAKOUT") {
    if (bk_ready) {
      if (paused) {
        display.setCursor(43, 28);
        display.print("PAUSED");
        display.display();
        delay(200);

      } else {
        newFrame = false;
        display.clearDisplay();

        // Draw lives
        for (int i = 0; i < lives; i++) {
          display.drawBitmap(120 - i * 5, 60, bmpBreakoutLife, 8, 4, SH110X_WHITE);
        }

        // Draw level
        display.setCursor(0, 56);
        display.print(level);



        bool allBlocksDestroyed = true;
        // Draw blocksbool allBlocksDestroyed = true;
        bool hasCollidedVX = false;  // Tracks whether a collision has occurred in this frame
        bool hasCollidedVY = false;  // Tracks whether a collision has occurred in this frame
        for (int i = 0; i < MAX_BLOCKS_Y; i++) {
          for (int j = 0; j < NUM_BLOCKS_X; j++) {
            Block& block = blocks[i][j];  // Reference to the block

            if (block.health > 0) {  // Only draw the block if it has health
              display.drawBitmap(block.x, block.y, block.bitmap, BLOCK_WIDTH, BLOCK_HEIGHT, SH110X_WHITE);
              allBlocksDestroyed = false;  // There's still a block left

              // Collision detection with blocks
              if (ballY < block.y + BLOCK_HEIGHT && ballY + 8 > block.y && ballX < block.x + BLOCK_WIDTH && ballX + 8 > block.x) {
                block.health--;  // Decrease block health

                // Change block bitmap based on health
                if (block.health == 2) {
                  block.bitmap = bmpBreakoutBlockB;
                } else if (block.health == 1) {
                  block.bitmap = bmpBreakoutBlockC;
                } else if (block.health <= 0) {
                  if (random(10) == 0 && numPowerups < MAX_POWERUPS) {
                    powerups[numPowerups].x = block.x + BLOCK_WIDTH / 2;
                    powerups[numPowerups].y = block.y + BLOCK_HEIGHT / 2;
                    powerups[numPowerups].vy = 1;
                    powerups[numPowerups].bitmap = bmpBreakoutPower;
                    numPowerups++;
                  }
                }
                if (ballX + 4 > block.x && ballX + 4 < block.x + BLOCK_WIDTH) {
                  hasCollidedVX = true;  // A collision has occurred
                } else {
                  hasCollidedVX = true;  // A collision has occurred
                }
              }
            }
          }
        }


        // If a collision has occurred, reverse the ball's direction
        if (hasCollidedVX) {
          ballVX = -ballVX;  // Reverse X velocity
        }

        if (hasCollidedVY) {
          ballVY = -ballVY;  // Reverse Y velocity
        }

        // If all blocks are destroyed, display "COMPLETE", pause the game, and reset the blocks
        if (allBlocksDestroyed) {
          display.setCursor(37, 28);
          display.print("COMPLETE");
          display.display();
          delay(1000);

          level++;  // Increase level
          for (int i = 0; i < MAX_BLOCKS_Y; i++) {
            for (int j = 0; j < NUM_BLOCKS_X; j++) {
              blocks[i][j].health = 0;
            }
          }

          // Reset blocks for the new level
          for (int i = 0; i < MAX_BLOCKS_Y; i++) {
            for (int j = 0; j < NUM_BLOCKS_X; j++) {
              blocks[i][j].health = (i < level && !(level == 4 && i < 3)) ? 3 : 0;
            }
          }
          // Reset ball and paddle positions
          ballX = 60;
          ballY = 49;
          ballVX = -2;
          ballVY = -2;
          paddleX = 56;
          firstFrame = true;
          // Pause the game
          delay(1000);
        }

        // Draw ball
        display.drawBitmap(ballX, ballY, bmpBreakoutBall, 8, 8, SH110X_WHITE);

        // Draw paddle
        // display.drawBitmap(paddleX, 58, paddleBitmap, 16, 4, SH110X_WHITE);


        // Update ball position
        ballX += ballVX;
        ballY += ballVY;

        // Collision detection with screen edges
        if (ballX < 0 || ballX > 120) {
          ballVX = -ballVX;  // Reverse X velocity
        }
        if (ballY < 0) {
          ballVY = -ballVY;  // Reverse Y velocity
        }

        // Collision detection with paddle
        if (ballY > 50 && ballX + 8 > paddleX && ballX < paddleX + paddleWidth) {
          // Calculate the difference between the center of the ball and the center of the paddle
          int diff = (ballX + 4) - (paddleX + 8);

          // Adjust the ball's X velocity based on the difference
          ballVX = diff / 4;

          // Make sure the ball's X velocity is at least 1, to prevent the ball from moving straight up
          //if (ballVX == 0) {
          //  ballVX = 1;
          //}

          ballVY = -ballVY;  // Reverse Y velocity
        }

        if (ballX > 120) {
          ballX = 119;
        }
        if (ballX < 0) {
          ballX = 1;
        }

        // Game over condition
        if (ballY > 56) {
          lives--;  // Decrease the number of lives

          if (lives > 0) {
            // Reset ball and paddle positions
            ballX = 60;
            ballY = 49;
            ballVX = -2;
            ballVY = -2;
            paddleX = 56;
            newFrame = true;
            firstFrame = true;
            newFrame = true;
            oldPos = encoder.getPosition();
          } else {
            display.setCursor(37, 28);
            display.print("Game Over");
            display.display();  //Delay avoids colliding when returning
            delay(1000);
            fillScreenRandomly(128, 4);
            delay(50);
            oldPos = encoder.getPosition();
            bk_ready = false;
            mode = "MAIN";  // Ends game and returns to main loop
          }
        }

        if (firstFrame && !newFrame) {
          display.setCursor(46, 28);
          display.print("READY?");
          display.display();
          delay(1000);
          firstFrame = false;
        }
        //powerup rendering
        for (int i = 0; i < numPowerups; i++) {
          PowerUp& powerup = powerups[i];  // Reference to the power-up

          // Update power-up position
          powerup.y += powerup.vy;

          // Draw power-up
          display.drawBitmap(powerup.x, powerup.y, powerup.bitmap, 4, 4, SH110X_WHITE);

          // Collision detection with paddle
          if (powerup.y + 4 > 58 && powerup.x + 4 > paddleX && powerup.x < paddleX + paddleWidth) {
            // Apply power-up effect
            if (paddleWidth == 32) {
              lives++;
            } else {
              paddleWidth = 32;
            }
            paddleX = paddleX - 8;

            // Remove power-up from array
            for (int j = i; j < numPowerups - 1; j++) {
              powerups[j] = powerups[j + 1];
            }
            numPowerups--;
            powerupTimer = millis() + 10000;  // Set the timer to 10 seconds from now
            i--;                              // Decrement i to account for the removed power-up
          }
        }
        //Powerup
        if (powerupTimer > 0) {
          display.drawBitmap(paddleX, 58, bmpBreakoutPaddle2, 32, 4, SH110X_WHITE);
          if (millis() > powerupTimer) {
            // Power-up has expired, reset paddle
            paddleWidth = 16;
            powerupTimer = 0;
          } else if (millis() > powerupTimer - 3000) {
            // Less than 3 seconds remaining, flash the paddle

            if (millis() - lastFlashTime > 500) {  // 500 ms = 2 flashes per second
              paddleFlashing = !paddleFlashing;
              lastFlashTime = millis();
            }
            if (paddleFlashing) {
              display.drawBitmap(paddleX, 58, bmpBreakoutPaddle2, 32, 4, SH110X_WHITE);
            } else {
              display.drawBitmap(paddleX, 58, bmpBreakoutPaddle2F, 32, 4, SH110X_WHITE);
            }
          }
        } else {
          display.drawBitmap(paddleX, 58, bmpBreakoutPaddle, 16, 4, SH110X_WHITE);
        }

        display.display();
        delay(40);
      }
    }
  }
}