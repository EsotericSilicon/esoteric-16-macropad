
//Todo: Find memory leak, and fix it so no variables from macropad.ino overlap, just do lo_ on all vars at this point
//ALso, double check initialization. 
//Ask jables for ways to optimize this and look for memory leaks. 

#define LO_NUM_BUTTONS 12
#define LO_NUM_NEOKEYS 4
#define LO_TOTAL_BUTTONS (LO_NUM_BUTTONS + LO_NUM_NEOKEYS)

const uint8_t bmpLightsoutOff[] = {
  0b01111110,  // Row 1
  0b10000001,  // Row 2
  0b10000001,  // Row 3
  0b10000001,  // Row 4
  0b10000001,  // Row 5
  0b10000001,  // Row 6
  0b10000001,  // Row 7
  0b01111110,  // Row 8
};
const uint8_t bmpLightsoutOn[] = {
  0b01111110,  // Row 1
  0b11111111,  // Row 2
  0b11111111,  // Row 3
  0b11111111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b11111111,  // Row 7
  0b01111110,  // Row 8
};

bool lo_lights[LO_TOTAL_BUTTONS] = { false };
bool lo_ready = false;
bool lo_neokeyState[LO_NUM_NEOKEYS] = { false };
bool lo_keyState[LO_NUM_BUTTONS] = { false };



void lightsoutLoopA() {
  display.clearDisplay();

  // Initialize the NeoPixels
  pixels.setBrightness(255);

  // Initialize all pixels to 'off'
  for (int i = 0; i < 12; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));  // Off
  }

  for (int i = 0; i < 4; i++) {
    neokey.pixels.setPixelColor(i, pixels.Color(0, 0, 0));  // Off
  }
  pixels.show();

  lo_lights[LO_TOTAL_BUTTONS] = { false };
  lo_ready = false;
  lo_neokeyState[LO_NUM_NEOKEYS] = { false };
  lo_keyState[LO_NUM_BUTTONS] = { false };


  display.clearDisplay();
  display.setCursor(0, 0);
  display.print("Lights Out!");
  display.setCursor(5, 32);
  display.print("Ready?");
  display.display();
  delay(1500);
  lo_ready = true;
  while (mode == "LIGHTSOUT") {
    display.clearDisplay();
    display.setCursor(0, 0);
    display.print("Lights Out!");
    display.setCursor(80, 32);
    display.print("Quit ->");
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        int index = i * 4 + j;
        int x = j * 10;         // Adjust these values as needed
        int y = 16 + (i * 10);  // Adjust these values as needed
        if (lo_lights[index]) {
          display.drawBitmap(x, y, bmpLightsoutOn, 8, 8, SH110X_WHITE);
        } else {
          display.drawBitmap(x, y, bmpLightsoutOff, 8, 8, SH110X_WHITE);
        }
      }
    }
    // Check if all lights are off
    bool allOff = true;
    for (int i = 0; i < LO_TOTAL_BUTTONS; i++) {
      if (lo_lights[i]) {
        allOff = false;
        break;
      }
    }

    if (!digitalRead(0)) {
      fillScreenRandomly(128, 4);
      delay(50);
      mode = "MAIN";
      delay(50);
    }
    if (allOff) {
      // All lights are off, display a "You win" message
      display.clearDisplay();
      display.setCursor(40, 28);
      display.print("You win!");
      display.display();

      // Wait for 3 seconds
      delay(3000);

      fillScreenRandomly(128, 4);
      delay(50);
      mode = "MAIN";
      delay(50);
    }
    display.display();
  }
}

void lightsoutLoopB() {
  while (mode == "LIGHTSOUT") {
    if (lo_ready) {
      // Check if key 0 is pressed
      // Check the state of each button
      for (uint8_t i = 0; i < LO_NUM_BUTTONS; i++) {
        bool isPressed = !digitalRead(i + 1);  // switch pressed!

        int logicalIndex = buttonOrder_b[i];
        if (isPressed != lo_keyState[i]) {
          // Button state has changed
          if (isPressed && !lo_keyState[i]) {  // Button is pressed and was not pressed before
            SerialTinyUSB.println("Key Pressed " + String(i) + ", Log:" + logicalIndex);
            lo_toggleLight(logicalIndex);
            lo_toggleNeighbors(logicalIndex);
          }
          lo_keyState[i] = isPressed;
        }
      }

      uint8_t buttons = neokey.read();
      for (uint8_t i = 0; i < LO_NUM_NEOKEYS; i++) {
        bool isPressed = buttons & (1 << i);
        //SerialTinyUSB.println("Neokey State " + String(i) + ":" + String(lo_neokeyState[i]));

        if (isPressed != lo_neokeyState[i]) {
          SerialTinyUSB.println("Neokey Change " + String(i));
          // Button state has changed
          lo_neokeyState[i] = isPressed;  // Update the button state immediately
          if (isPressed) {                // Button is pressed
            int logicalIndex = buttonOrder_b[LO_NUM_BUTTONS + i];
            SerialTinyUSB.println("Neokey Pressed " + String(i) + ", Log:" + logicalIndex);
            lo_toggleLight(logicalIndex);
            lo_toggleNeighbors(logicalIndex);
          }
        }
        //SerialTinyUSB.println("Neokey State " + String(i) + ":" + String(lo_neokeyState[i]));
        //lo_neokeyState[i] = isPressed;
      }



      // Update the NeoPixels
      pixels.show();
      neokey.pixels.show();
      delay(100);
    }
  }
}

void lo_toggleLight(int logicalIndex) {
  // Toggle the light at the given index
  int physicalIndex = buttonOrder_a[logicalIndex];
  lo_lights[logicalIndex] = !lo_lights[logicalIndex];


  // Update the corresponding NeoPixel
  if (physicalIndex < LO_NUM_BUTTONS) {
    // This is a regular button, update the corresponding NeoPixel
    if (lo_lights[logicalIndex]) {
      pixels.setPixelColor(physicalIndex, pixels.Color(255, 0, 0));  // Red
    } else {
      pixels.setPixelColor(physicalIndex, pixels.Color(0, 0, 0));  // Off
    }
  } else {
    // This is a NeoKey, update the corresponding NeoPixel on the NeoKey
    if (lo_lights[logicalIndex]) {
      neokey.pixels.setPixelColor(3 - (physicalIndex - LO_NUM_BUTTONS), pixels.Color(255, 0, 0));  // Red
      //neokey.pixels.setPixelColor((physicalIndex - LO_NUM_BUTTONS), pixels.Color(255, 0, 0));  // Red
    } else {
      neokey.pixels.setPixelColor(3 - (physicalIndex - LO_NUM_BUTTONS), pixels.Color(0, 0, 0));  // Off
      //neokey.pixels.setPixelColor((physicalIndex - LO_NUM_BUTTONS), pixels.Color(0, 0, 0));  // Off
    }
  }
  // Update the NeoPixels
  pixels.show();
  neokey.pixels.show();
}

void lo_toggleNeighbors(int logicalIndex) {
  // Toggle the neighbors if they are within the grid
  if (logicalIndex > 0 && logicalIndex != 4 && logicalIndex != 8 && logicalIndex != 12) {
    lo_toggleLight(logicalIndex - 1);  // Left
    SerialTinyUSB.println(" + LEFT " + String(logicalIndex - 1));
  }
  if (logicalIndex < 15 && logicalIndex != 3 && logicalIndex != 7 && logicalIndex != 11) {
    lo_toggleLight(logicalIndex + 1);  // Right
    SerialTinyUSB.println(" + RIGHT " + String(logicalIndex + 1));
  }
  if (logicalIndex > 3) {
    lo_toggleLight(logicalIndex - 4);  // Above
    SerialTinyUSB.println(" + UP " + String(logicalIndex - 4));
  }
  if (logicalIndex < 12) {
    lo_toggleLight(logicalIndex + 4);  // Below
    SerialTinyUSB.println(" + DOWN " + String(logicalIndex + 4));
  }
}
