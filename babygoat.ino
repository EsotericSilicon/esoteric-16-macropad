// Baby goat sprites
const uint8_t bg_goatRun1[] = {
  // 8x8 bitmap of a baby goat running (frame 1)
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b00111101,  // Row 8
};

const uint8_t bg_goatRun2[] = {
  // 8x8 bitmap of a baby goat running (frame 2)
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b01111100,  // Row 8
};

const uint8_t bg_goatRun3[] = {
  // 8x8 bitmap of a baby goat running (frame 3)
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b00111110,  // Row 8
};

const uint8_t bg_goatRun4[] = {
  // 8x8 bitmap of a baby goat running (frame 4)
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b10111100,  // Row 8
};

const uint8_t bg_goatJump[] = {
  // 8x8 bitmap of a baby goat jumping
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11010111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b10000001,  // Row 8
};

const uint8_t bg_goatHeadbutt[] = {
  // 8x8 bitmap of a baby goat headbutting
  0b00111101,  // Row 1
  0b01111111,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b10111101,  // Row 8
};

// Obstacle sprites
const uint8_t bg_obstacle1[] = {
  // 8x8 bitmap of an obstacle
  0b00011100,  // Row 1
  0b01011110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11011111,  // Row 5
  0b11011111,  // Row 6
  0b01011110,  // Row 7
  0b00011100,  // Row 8
};

const uint8_t bg_obstacle2[] = {
  // 8x8 bitmap of another obstacle
  0b00111100,  // Row 1
  0b01111100,  // Row 2
  0b11001101,  // Row 3
  0b11011101,  // Row 4
  0b11111101,  // Row 5
  0b11111101,  // Row 6
  0b01111100,  // Row 7
  0b00111100,  // Row 8
};

// Background element sprites
const uint8_t bg_background1[] = {
  // 8x8 bitmap of a background element
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11000000,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11111111,  // Row 6
  0b01111110,  // Row 7
  0b00111100,  // Row 8
};

const uint8_t bg_background2[] = {
  // 8x8 bitmap of another background element
  0b00111100,  // Row 1
  0b01111110,  // Row 2
  0b11001111,  // Row 3
  0b11011111,  // Row 4
  0b11111111,  // Row 5
  0b11000000,  // Row 6
  0b01111110,  // Row 7
  0b00111100,  // Row 8
};

// Game state
bool bg_jumping = false;
bool bg_ready = false;
bool bg_headbutting = false;
int bg_obstacleX = 128;  // Start the obstacle off the right side of the screen
bool bg_gameOver = false;
int bg_goatY = 56;                   // Start the goat at the bottom of the screen
unsigned long bg_jumpStart = 0;      // The time when the jump started
unsigned long bg_headbuttStart = 0;  // The time when the headbutt started
int bg_goatX = 0;                    // The goat's horizontal position
int bg_obstacle2X = 200;             // Start the second obstacle off the right side of the screen
int bg_obstacle2Speed = 2;           // The second obstacle moves twice as fast

int bg_goatWidth = 8;
int bg_goatHeight = 8;
int bg_obstacle1Width = 8;
int bg_obstacle1Height = 8;
int bg_obstacle2Width = 8;
int bg_obstacle2Height = 8;

bool bg_buttonState[3] = { false, false, false };
bool bg_lastButtonState[3] = { false, false, false };
unsigned long bg_lastDebounceTime[3] = { 0, 0, 0 };
unsigned long bg_debounceDelay = 50;  // Debounce delay in milliseconds


int bg_obstacle1HeightMod[8] = { 4, 3, 2, 1, 1, 2, 3, 4 };
int bg_obstacle2HeightMod[8] = { 4, 3, 2, 1, 1, 2, 3, 4 };



void bg_gameLoopA() {
  bg_ready = false;  //Set all variables initial state here, since we can't guarantee this is the first run.
  bg_jumping = false;
  bg_headbutting = false;
  bg_obstacleX = 128;  // Used to let the other loop know it's safe to run.
  bg_gameOver = false;
  bg_goatY = 56;          // Start the goat at the bottom of the screen
  bg_jumpStart = 0;       // The time when the jump started
  bg_headbuttStart = 0;   // The time when the headbutt started
  bg_goatX = 0;           // The goat's horizontal position
  bg_obstacle2X = 200;    // Start the second obstacle off the right side of the screen
  bg_obstacle2Speed = 2;  // The second obstacle moves twice as fast
  for (int i = 0; i < 3; i++) {
    bg_buttonState[i] = false;
    bg_lastButtonState[i] = false;
  }

  display.clearDisplay();
  delay(250);       // Give loop B a moment to be sure it's ready
  bg_ready = true;  // Used to let the other loop know it's safe to run.

  while (mode == "BABYGOAT") {

    if (bg_gameOver) {
      display.setCursor(37, 28);
      display.print("Game Over");
      display.display();  //Delay avoids colliding when returning
      delay(1000);
      fillScreenRandomly(128, 4);
      delay(50);
      mode = "MAIN";        // End the game
      bg_gameOver = false;  // Reset the game over state
      return;
    }

    display.clearDisplay();  // Keep display, neopixel on one loop to avoid collisions.

    // Draw the baby goat
    const uint8_t* sprite;
    if (bg_jumping || bg_goatY < 56) {
      sprite = bg_goatJump;
    } else if (bg_headbutting) {
      sprite = bg_goatHeadbutt;
    } else {
      // Choose a running sprite based on the current frame
      int frame = (millis() / 200) % 4;  // Change sprite every 200 ms
      if (frame == 0) {
        sprite = bg_goatRun1;
      } else if (frame == 1) {
        sprite = bg_goatRun2;
      } else if (frame == 2) {
        sprite = bg_goatRun3;
      } else {
        sprite = bg_goatRun4;
      }
    }
    display.drawBitmap(bg_goatX, bg_goatY, sprite, bg_goatWidth, bg_goatHeight, SH110X_WHITE);  // Draw the sprite at the bottom-left corner of the screen
    display.drawBitmap(bg_obstacleX, 56, bg_obstacle1, bg_obstacle1Width, bg_obstacle1Height, SH110X_WHITE);
    display.drawBitmap(bg_obstacle2X, 56, bg_obstacle2, bg_obstacle2Width, bg_obstacle2Height, SH110X_WHITE);

    // Update the game state
    bg_jumping = false;      // The baby goat can only jump for one frame
    bg_headbutting = false;  // The baby goat can only headbutt for one frame
    bg_obstacleX--;          // Move the obstacle to the left

    if (bg_obstacleX < -8) {
      bg_obstacleX = 128;  // Move the obstacle back to the right side of the screen when it goes off the left side
    }

    // Move the second obstacle to the left
    bg_obstacle2X -= bg_obstacle2Speed;
    if (bg_obstacle2X < -8) {
      bg_obstacle2X = 200;  // Move the second obstacle back to the right side of the screen when it goes off the left side
    }



    display.display();
    delay(25);
  }
}

void bg_gameLoopB() {
  while (mode == "BABYGOAT") {
    if (bg_ready) {

      // Read the button states
      for (int i = 0; i < 3; i++) {
        bool reading = !digitalRead(i);

        // Check if the button state has changed
        if (reading != bg_lastButtonState[i]) {
          // Reset the debounce timer
          bg_lastDebounceTime[i] = millis();
        }

        // Check if the button state has been stable for longer than the debounce delay
        if ((millis() - bg_lastDebounceTime[i]) > bg_debounceDelay) {
          // If the button state has changed, update the button state and perform the corresponding action
          if (reading != bg_buttonState[i]) {
            bg_buttonState[i] = reading;

            if (reading) {
              if (i == 0) {
                bg_gameOver = true;
              } else if (i == 1 && bg_goatY >= 50) {  //56
                bg_jumpStart = millis();              // Start the jump
              } else if (i == 2) {
                bg_headbuttStart = millis();  // Start the headbutt
              }
            }
          }
        }

        bg_lastButtonState[i] = reading;
      }

      // Check for collision with the first obstacle
      if (bg_obstacleX < bg_goatX + bg_goatWidth && bg_goatY >= 56 - bg_obstacle1HeightMod[bg_obstacleX - bg_goatX]) {
        // The baby goat hit the first obstacle
        if (bg_headbuttStart > 0 && millis() - bg_headbuttStart < 300) {
          // The goat is headbutting, so reset the obstacle
          bg_obstacleX = 128;
        } else {
          // The goat is not headbutting, so end the game
          bg_gameOver = true;
        }
      }

      // Check for collision with the second obstacle
      if (bg_obstacle2X < bg_goatX + bg_goatWidth && bg_goatY >= 56 - bg_obstacle2HeightMod[bg_obstacle2X - bg_goatX]) {
        // The baby goat hit the second obstacle
        bg_gameOver = true;
      }

      // Move the goat up and down if it's jumping
      if (bg_jumpStart > 0) {
        unsigned long t = millis() - bg_jumpStart;
        if (t < 200) {
          // The goat moves up quickly in the first 200 ms
          bg_goatY = 56 - t / 10;
        } else if (t < 600) {
          // The goat floats in the air for the next 400 ms
          bg_goatY = 56 - 20;
        } else if (t < 1000) {
          // Then the goat moves down slower in the next 400 ms
          bg_goatY = 56 - 20 + (t - 600) / 8;
        } else {
          // The jump is over
          bg_jumpStart = 0;
          bg_goatY = 56;
        }
      }

      // Prevent the goat from moving past the ground
      if (bg_goatY > 56) {
        bg_goatY = 56;
      }

      // Move the goat forward and back if it's headbutting
      if (bg_headbuttStart > 0) {
        unsigned long t = millis() - bg_headbuttStart;
        if (t < 150) {
          // The goat moves forward quickly in the first 150 ms
          bg_goatX = t / 5;
        } else if (t < 300) {
          // Then the goat moves back quickly in the next 150 ms
          bg_goatX = (300 - t) / 5;
        } else {
          // The headbutt is over
          bg_headbuttStart = 0;
          bg_goatX = 0;
        }
      }

      delay(25);
    }
  }
}
