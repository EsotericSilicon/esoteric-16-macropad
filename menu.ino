
// Menu state
bool menuOpen = false;
int menuIndex = 0;
int cursorIndex = 0;


// Menu items
String menuDisplayNames[] = { "Invert Scr", "Theme", "Brightness", "Volume", "Caps FX", "Wipe Data", "Btn FX", "Color 1", "Color 1 Lum", "Color 2", "Color 2 Lum", "Color Speed", "Color Sp Caps", "Dbg Key(1)", "Dbg CEC(3)", "T. Out", "T. In", "T. Menu" };
int numMenuItems = sizeof(menuDisplayNames) / sizeof(menuDisplayNames[0]);
bool menuEncMod = false;
uint8_t debugKeycode = HID_KEY_NONE;
uint16_t debugKeycodeCEC = HID_USAGE_CONSUMER_POWER;

enum MenuItem {
  INVERT_SCREEN,
  THEME,
  BRIGHTNESS,
  VOLUME,
  CAPS_EFFECT,
  WIPE_DATA,
  BUTTON_EFFECT,
  COLOR_1,
  COLOR_1_LUM,
  COLOR_2,
  COLOR_2_LUM,
  COLOR_SPEED,
  COLOR_SPEED_CAPS,
  DEBUG_KEYCODES,
  DEBUG_KEYCODES_CEC,
  TRAN_OUT,
  TRAN_IN,
  TRAN_MENU_IN,
  NUM_MENU_ITEMS  // Always keep this as the last item
};


void valueUp() {
  changeValue(true);
}

void valueDown() {
  changeValue(false);
}

void menuUp() {
  if (!menuOpen) return;
  cursorIndex--;
  if (cursorIndex < 0) cursorIndex = 0;                  // Don't go past the start of the menuItems array
  if (cursorIndex < menuIndex) menuIndex = cursorIndex;  // Scroll up if the cursor is above the window
  drawMenu();
}

void menuDown() {
  if (!menuOpen) return;
  cursorIndex++;
  if (cursorIndex >= numMenuItems) cursorIndex = numMenuItems - 1;  // Don't go past the end of the menuItems array
  if (cursorIndex >= menuIndex + 4) menuIndex = cursorIndex - 3;    // Scroll down if the cursor is below the window
  drawMenu();
}

void drawMenu() {
  if (!menuOpen) return;
  SerialTinyUSB.println("DrawMenu");

  // Draw a black rectangle with a white outline
  int x = display.width() * 0.05;
  int y = 12;
  int w = display.width() * 0.9;
  int h = display.height() * 4 / 5;
  display.fillRect(x, y, w, h, SH110X_BLACK);
  display.drawRect(x, y, w, h, SH110X_WHITE);

  //Draw the title
  display.fillRect(30, 0, 60, 12, SH110X_WHITE);
  int titleWidth = pages[currentPage]->title.length() * 6;  // 6 is the width of a character
  int titleStartX = 35;                                     // Center the title

  display.setCursor(titleStartX, 1);                 // Shift the title down by 1 pixel
  display.setTextColor(SH110X_BLACK, SH110X_WHITE);  // black text, white background
  display.print("Settings");                         // This line prints the title of the current page
  display.setTextColor(SH110X_WHITE, SH110X_BLACK);  // black text, white background


  // Draw the scrollbar
  int scrollbarWidth = 10;  // Adjust as needed
  int scrollbarX = x + w - scrollbarWidth;
  display.drawRect(scrollbarX, y, scrollbarWidth, h, SH110X_WHITE);

  // Draw the scrollbar handle
  int visibleItems = 4;  // The number of menu items that can be visible at once
  int handleHeight = h * visibleItems / numMenuItems;
  int handleY = y + (h - handleHeight) * menuIndex / (numMenuItems - visibleItems);
  display.fillRect(scrollbarX, handleY, scrollbarWidth, handleHeight, SH110X_WHITE);

  // Draw the menu items
  int itemHeight = h / visibleItems;
  for (int i = 0; i < visibleItems; i++) {
    int index = menuIndex + i;
    if (index >= numMenuItems) break;  // Don't go past the end of the menuItems array

    display.setCursor(x + 12, y + itemHeight * i + itemHeight / 2 - 3);
    display.print(menuDisplayNames[index]);

    // Draw the current value of the setting
    String value;
    if (index == INVERT_SCREEN) {
      value = invertScreen ? "On" : "Off";
    } else if (index == THEME) {
      value = themes[themeIndex];
    } else if (index == BUTTON_EFFECT) {
      value = buttonEffect;
    } else if (index == BRIGHTNESS) {
      value = String(brightness);
    } else if (index == VOLUME) {
      value = String(volume);
    } else if (index == CAPS_EFFECT) {
      value = capsEffect ? "On" : "Off";

    } else if (index == WIPE_DATA) {
      value = wipeData ? "Yes" : "No";

    } else if (index == COLOR_1) {
      value = String(color1);

    } else if (index == COLOR_1_LUM) {
      value = String(color1Lum);

    } else if (index == COLOR_2) {
      value = String(color2);

    } else if (index == COLOR_2_LUM) {
      value = String(color2Lum);
    } else if (index == COLOR_SPEED) {
      value = String(colorSpeed);
    } else if (index == COLOR_SPEED_CAPS) {
      value = String(colorSpeedCaps);
    } else if (index == DEBUG_KEYCODES) {
      value = "0x" + String(debugKeycode, HEX);
    } else if (index == DEBUG_KEYCODES_CEC) {
      value = "0x" + String(debugKeycodeCEC, HEX);
    } else if (index == TRAN_OUT) {
      value = tranOutList[tranOutListIndex];
    } else if (index == TRAN_IN) {
      value = tranInList[tranInListIndex];
    } else if (index == TRAN_MENU_IN) {
      value = menuInList[menuInListIndex];
    }
    display.setCursor(x + w - 1 - value.length() * 6 - scrollbarWidth, y + itemHeight * i + itemHeight / 2 - 3);
    display.print(value);

    // Draw a cursor next to the selected menu item
    if (index == cursorIndex) {
      if (!menuEncMod) {
        //display.drawFastHLine(x + 3, 5 + y + itemHeight * i, 4, SH110X_WHITE);
        display.drawBitmap(x + 2, 2 + y + itemHeight * i, bmpMenuLR, 16, 9, SH110X_WHITE);
      } else {
        // display.drawFastVLine(x + 5, y + itemHeight * i, itemHeight, SH110X_WHITE);
        display.drawBitmap(x + 2, 2 + y + itemHeight * i, bmpMenuUD, 16, 9, SH110X_WHITE);
      }
    }
  }

  display.display();
}


void menuToggle() {

  SerialTinyUSB.println("MenuToggle");
  if (!menuOpen) {
    menuOpen = true;
    fillScreenRandomly();  //Todo: Make this an option for background effect.
    if (tranMenuIn == "box") {
      drawExpandingRectangles();
    }
    drawMenu();
  } else {
    menuClose();
  }
}

void menuClose() {
  SerialTinyUSB.println("MenuClose");
  // Draw a black rectangle with a white outline
  int x = display.width() * 0.05;
  int y = 12;
  int w = display.width() * 0.9;
  int h = display.height() * 4 / 5;
  display.fillRect(x, y, w, h, SH110X_BLACK);
  display.drawRect(x, y, w, h, SH110X_WHITE);
  display.setCursor(x + 2, y + 2);
  display.print("Saving...");

  display.display();
  delay(100);
  // Save the settings to Flash
  //Settings settings = { magicFlag, invertScreen, theme, brightness, volume, capsEffect, wipeData, buttonEffect, color1, color1Lum, color2, color2Lum, colorSpeed, colorSpeedCaps };




  // Create a Settings object with the current settings
  Settings newSettings;
  magicFlag.toCharArray(newSettings.magicFlag, sizeof(newSettings.magicFlag));
  theme.toCharArray(newSettings.theme, sizeof(newSettings.theme));
  newSettings.invertScreen = invertScreen;
  newSettings.brightness = brightness;
  newSettings.volume = volume;
  newSettings.capsEffect = capsEffect;
  newSettings.wipeData = wipeData;
  buttonEffect.toCharArray(newSettings.buttonEffect, sizeof(newSettings.buttonEffect));
  newSettings.color1 = color1;
  newSettings.color1Lum = color1Lum;
  newSettings.color2 = color2;
  newSettings.color2Lum = color2Lum;
  newSettings.colorSpeed = colorSpeed;
  newSettings.colorSpeedCaps = colorSpeedCaps;
  tranOut.toCharArray(newSettings.tranOut, sizeof(newSettings.tranOut));
  tranIn.toCharArray(newSettings.tranIn, sizeof(newSettings.tranIn));
  tranMenuIn.toCharArray(newSettings.tranMenuIn, sizeof(newSettings.tranMenuIn));

  // Compare the new settings with the last saved settings
  if (strcmp(newSettings.magicFlag, settings.magicFlag) != 0 || strcmp(newSettings.theme, settings.theme) != 0 || strcmp(newSettings.tranMenuIn, settings.tranMenuIn) != 0 || strcmp(newSettings.tranIn, settings.tranIn) != 0 || strcmp(newSettings.tranOut, settings.tranOut) != 0 || strcmp(newSettings.buttonEffect, settings.buttonEffect) != 0 || newSettings.invertScreen != settings.invertScreen || newSettings.brightness != settings.brightness || newSettings.volume != settings.volume || newSettings.capsEffect != settings.capsEffect || newSettings.wipeData != settings.wipeData || newSettings.color1 != settings.color1 || newSettings.color1Lum != settings.color1Lum || newSettings.color2 != settings.color2 || newSettings.color2Lum != settings.color2Lum || newSettings.colorSpeed != settings.colorSpeed || newSettings.colorSpeedCaps != settings.colorSpeedCaps) {
    // The settings have changed, so save the new settings
    settings = newSettings;
    SerialTinyUSB.println("Saving " + String(sizeof(settings)) + " bytes...");
    noInterrupts();
    EEPROM.begin(sizeof(settings));
    EEPROM.put(0, settings);
    bool success = EEPROM.commit();
    EEPROM.end();
    interrupts();
    display.fillRect(x, y, w, h, SH110X_BLACK);
    display.drawRect(x, y, w, h, SH110X_WHITE);
    if (!success) {
      SerialTinyUSB.println("Failed to commit changes to Flash");
      display.setCursor(x + 2, y + 2);
      display.print("Error Saving!");
    } else {
      SerialTinyUSB.println("Save success!");
      display.setCursor(x + 2, y + 2);
      display.print("Success!");
    }
    display.display();
    delay(1000);
  }
  //todo: Make unique setting for menu out

  if (tranOut == "slide") {
    slide_out();
  } else if (tranOut == "pixel") {
    pixelate_out();
  } else if (tranOut == "swirl") {
    swirl_out();
  } else if (tranOut == "swirlZ") {
    swirl_scale_out();
  } else if (tranOut == "zoom") {
    shrink_out();
  } else if (tranOut == "Hlines") {
    fillScreenRandomly();
  }
  menuOpen = false;
  pageChanged = true;
  display.clearDisplay();
  display.display();
}


void changeValue(bool increase) {
  if (!menuOpen) return;
  switch (cursorIndex) {
    case INVERT_SCREEN:
      invertScreen = !invertScreen;
      display.invertDisplay(invertScreen);  // Invert the display colors
      break;
    case THEME:
      if (increase) {
        themeIndex++;
        if (themeIndex >= numThemes) themeIndex = 0;  // Wrap around to the first theme
      } else {
        themeIndex--;
        if (themeIndex < 0) themeIndex = numThemes - 1;  // Wrap around to the last theme
      }
      theme = themes[themeIndex];  // Update the theme string
      break;
    case TRAN_OUT:
      if (increase) {
        tranOutListIndex++;
        if (tranOutListIndex >= numtranOutList) tranOutListIndex = 0;  // Wrap around to the first theme
      } else {
        tranOutListIndex--;
        if (tranOutListIndex < 0) tranOutListIndex = numtranOutList - 1;  // Wrap around to the last theme
      }
      tranOut = tranOutList[tranOutListIndex];  // Update the theme string
      break;
    case TRAN_IN:
      if (increase) {
        tranInListIndex++;
        if (tranInListIndex >= numtranInList) tranInListIndex = 0;  // Wrap around to the first theme
      } else {
        tranInListIndex--;
        if (tranInListIndex < 0) tranInListIndex = numtranInList - 1;  // Wrap around to the last theme
      }
      tranIn = tranInList[tranInListIndex];  // Update the theme string
      break;
    case TRAN_MENU_IN:
      if (increase) {
        menuInListIndex++;
        if (menuInListIndex >= nummenuInList) menuInListIndex = 0;  // Wrap around to the first theme
      } else {
        menuInListIndex--;
        if (menuInListIndex < 0) menuInListIndex = nummenuInList - 1;  // Wrap around to the last theme
      }
      tranMenuIn = menuInList[menuInListIndex];  // Update the theme string
      break;
    case BRIGHTNESS:
      if (increase) {
        brightness += 1;
        if (brightness > 100) brightness = 100;
      } else {
        brightness -= 1;
        if (brightness < 0) brightness = 0;
      }
      break;
    case VOLUME:
      if (increase) {
        volume++;
        if (volume > 11) volume = 11;
      } else {
        volume--;
        if (volume < 0) volume = 0;
      }
      break;
    case COLOR_1:
      if (increase) {
        color1++;
        if (color1 > 255) color1 = 0;
      } else {
        color1--;
        if (color1 < 0) color1 = 255;
      }
      break;
    case COLOR_1_LUM:
      if (increase) {
        color1Lum++;
        if (color1Lum > 255) color1Lum = 0;
      } else {
        color1Lum--;
        if (color1Lum < 0) color1Lum = 255;
      }
      break;
    case COLOR_2:
      if (increase) {
        color2++;
        if (color2 > 255) color2 = 0;
      } else {
        color2--;
        if (color2 < 0) color2 = 255;
      }
      break;
    case COLOR_2_LUM:
      if (increase) {
        color2Lum++;
        if (color2Lum > 255) color2Lum = 0;
      } else {
        color2Lum--;
        if (color2Lum < 0) color2Lum = 255;
      }
      break;
    case COLOR_SPEED:
      if (increase) {
        colorSpeed++;
        if (colorSpeed > 255) colorSpeed = 0;
      } else {
        colorSpeed--;
        if (colorSpeed < 0) colorSpeed = 255;
      }
      break;
    case COLOR_SPEED_CAPS:
      if (increase) {
        colorSpeedCaps++;
        if (colorSpeedCaps > 255) colorSpeedCaps = 0;
      } else {
        colorSpeedCaps--;
        if (colorSpeedCaps < 0) colorSpeedCaps = 255;
      }
      break;
    case CAPS_EFFECT:
      capsEffect = !capsEffect;
      break;
    case WIPE_DATA:
      NVIC_SystemReset();  // Reset the microcontroller
      break;
    case BUTTON_EFFECT:
      if (increase) {
        if (buttonEffect == "Dots") {
          buttonEffect = "Solid";
        } else if (buttonEffect == "Solid") {
          buttonEffect = "Flash";
        } else {
          buttonEffect = "Dots";
        }
      } else {
        if (buttonEffect == "Dots") {
          buttonEffect = "Flash";
        } else if (buttonEffect == "Solid") {
          buttonEffect = "Dots";
        } else {
          buttonEffect = "Solid";
        }
      }
      break;
    case DEBUG_KEYCODES:
      if (increase) {
        // Increase the debug keycode
        debugKeycode++;
        if (debugKeycode > HID_KEY_GUI_RIGHT) debugKeycode = HID_KEY_NONE;  // Wrap around to the first keycode
      } else {
        // Decrease the debug keycode
        if (debugKeycode == HID_KEY_NONE) {
          debugKeycode = HID_KEY_GUI_RIGHT;  // Wrap around to the last keycode
        } else {
          debugKeycode--;
        }
      }
    case DEBUG_KEYCODES_CEC:
      if (increase) {
        // Increase the debug keycode
        debugKeycodeCEC++;
        if (debugKeycodeCEC > HID_USAGE_CONSUMER_AC_PAN) debugKeycodeCEC = HID_USAGE_CONSUMER_CONTROL;  // Wrap around to the first keycode
      } else {
        // Decrease the debug keycode
        if (debugKeycodeCEC == HID_USAGE_CONSUMER_CONTROL) {
          debugKeycodeCEC = HID_USAGE_CONSUMER_AC_PAN;  // Wrap around to the last keycode
        } else {
          debugKeycodeCEC--;
        }
      }
      break;
      //case FONT:
      //TODO: Logic
      //display.setFont(&CustomFont);  // Replace with the name of your font
  }
  drawMenu();
}
