#ifndef PAGE_H
#define PAGE_H

#include <Arduino.h>

class Button {
public:
  String name;
  int param;
  String altName;
  bool repeat;

  Button(String name, int param, String altName = "", bool repeat = false) 
    : name(name), param(param), altName(altName), repeat(repeat) {}
};

class Page {
public:
  String title;
  Button* buttons[16];

  Page(String title) : title(title) {}

  void addButton(Button* button) {
    for (int i = 0; i < 16; i++) {
      if (buttons[i] == NULL) {
        buttons[i] = button;
        break;
      }
    }
  }
};

#endif