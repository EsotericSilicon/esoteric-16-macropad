// Create pages
const int numPages = 11;
Page pageWindows("Windows");
Page pageLinux("Linux");
Page pageMacOS("Mac OS");
Page pageKeypad("Keypad");
Page pageDev("Dev");
Page pageApps("Apps");
Page pageMeetings("Meetings");
Page pageStreaming("Streaming");
Page pagePS5("PS5");
Page pageHacks("Hacks");
Page pageTest("Test");
Page* pages[numPages] = { &pageKeypad, &pageWindows, &pageLinux, &pageMacOS, &pageDev, &pageApps, &pageMeetings, &pageStreaming, &pagePS5, &pageHacks, &pageTest };


void setupPages(){
   // Setup Pages of Hotkeys. TStub that generates 16 identical keys for testing
  for (int i = 0; i < 16; i++) {
    pageLinux.buttons[i] = new Button("Key" + String(i), i);
  }
  //Windows
  pageWindows.buttons[buttonOrder_a[0]] = new Button("Emoji", 125);
  pageWindows.buttons[buttonOrder_a[1]] = new Button("TchPd", 128);
  pageWindows.buttons[buttonOrder_a[2]] = new Button("Shut.", 126);
  pageWindows.buttons[buttonOrder_a[3]] = new Button("Pres.", 123);

  pageWindows.buttons[buttonOrder_a[4]] = new Button("Vol-", 8);
  pageWindows.buttons[buttonOrder_a[5]] = new Button("Vol+", 9);
  pageWindows.buttons[buttonOrder_a[6]] = new Button("Mute", 10);
  pageWindows.buttons[buttonOrder_a[7]] = new Button("Cast", 121);

  pageWindows.buttons[buttonOrder_a[8]] = new Button("Copy", 12);
  pageWindows.buttons[buttonOrder_a[9]] = new Button("Cut", 13);
  pageWindows.buttons[buttonOrder_a[10]] = new Button("Paste", 14);
  pageWindows.buttons[buttonOrder_a[11]] = new Button("Clip.", 127);

  pageWindows.buttons[buttonOrder_a[12]] = new Button("sfc+", 5);
  pageWindows.buttons[buttonOrder_a[13]] = new Button("Sett.", 121);
  pageWindows.buttons[buttonOrder_a[14]] = new Button("Run", 124);
  pageWindows.buttons[buttonOrder_a[15]] = new Button("Lock", 122);




  //Linux
  pageLinux.buttons[0] = new Button("a", 0);
  pageLinux.buttons[1] = new Button("b", 1);
  pageLinux.buttons[2] = new Button("c", 2);
  pageLinux.buttons[3] = new Button("Vol-", 8);
  pageLinux.buttons[4] = new Button("Vol+", 9);
  pageLinux.buttons[5] = new Button("Mute", 10);
  pageLinux.buttons[6] = new Button("", -1);
  pageLinux.buttons[7] = new Button("", -1);
  pageLinux.buttons[8] = new Button("", -1);
  pageLinux.buttons[9] = new Button("", -1);
  pageLinux.buttons[10] = new Button("ls-l", 4);
  pageLinux.buttons[11] = new Button("", -1);
  pageLinux.buttons[12] = new Button("d", 3);
  pageLinux.buttons[13] = new Button("", 7);
  ;
  pageLinux.buttons[14] = new Button("", 6);
  pageLinux.buttons[15] = new Button("", -1);

  //Numpad
  pageKeypad.buttons[0] = new Button("NmLk", 100);
  pageKeypad.buttons[1] = new Button("/", 101);
  pageKeypad.buttons[2] = new Button("*", 102);
  pageKeypad.buttons[3] = new Button("7", 97, "Home");
  pageKeypad.buttons[4] = new Button("8", 98, "^");
  pageKeypad.buttons[5] = new Button("9", 99, "PgUp");
  pageKeypad.buttons[6] = new Button("4", 94, "<");
  pageKeypad.buttons[7] = new Button("5", 95, " ");
  pageKeypad.buttons[8] = new Button("6", 96, ">");
  pageKeypad.buttons[9] = new Button("1", 91, "End");
  pageKeypad.buttons[10] = new Button("2", 92, "v");
  pageKeypad.buttons[11] = new Button("3", 93, "PgDn");
  pageKeypad.buttons[12] = new Button("-", 103);
  pageKeypad.buttons[13] = new Button("+", 104);
  pageKeypad.buttons[14] = new Button("0", 90, "Inst");
  pageKeypad.buttons[15] = new Button("Ent", 105);

  //Mac os
  pageMacOS.buttons[buttonOrder_a[0]] = new Button("Spot", 109);
  pageMacOS.buttons[buttonOrder_a[1]] = new Button("Close", 110);
  pageMacOS.buttons[buttonOrder_a[2]] = new Button("Quit", 111);
  pageMacOS.buttons[buttonOrder_a[3]] = new Button("", -1);
  pageMacOS.buttons[buttonOrder_a[4]] = new Button("Cut", 112);
  pageMacOS.buttons[buttonOrder_a[5]] = new Button("Copy", 113);
  pageMacOS.buttons[buttonOrder_a[6]] = new Button("Paste", 114);
  pageMacOS.buttons[buttonOrder_a[7]] = new Button("", -1);
  pageMacOS.buttons[buttonOrder_a[8]] = new Button("Undo", -1);
  pageMacOS.buttons[buttonOrder_a[9]] = new Button("Redo", -1);
  pageMacOS.buttons[buttonOrder_a[10]] = new Button("New", 115);
  pageMacOS.buttons[buttonOrder_a[11]] = new Button("Save", 116);
  pageMacOS.buttons[buttonOrder_a[12]] = new Button("Unlk", 119);
  pageMacOS.buttons[buttonOrder_a[13]] = new Button("", -1);
  pageMacOS.buttons[buttonOrder_a[14]] = new Button("", -1);
  pageMacOS.buttons[buttonOrder_a[15]] = new Button("Say", 117);

  //Audio

  pageTest.buttons[0] = new Button("Beep", 107);
  pageTest.buttons[1] = new Button("PCM", 108);
  pageTest.buttons[2] = new Button("Alarm", 118);

  //Apps
  pageApps.buttons[0] = new Button("BkOut", 120);
  pageApps.buttons[1] = new Button("LtOut", 129);
  pageApps.buttons[2] = new Button("BGoat", 130);
  // pageApps.buttons[3] = new Button("YLYL", 131);
  pageApps.buttons[15] = new Button("Test", 999);
}


void drawPage() {
  if (menuOpen) return;  //Patch for something rendering the page over the menu
  SerialTinyUSB.println("DrawPage");
  display.clearDisplay();

  int titleWidth = pages[currentPage]->title.length() * 6;  // Diagonal lines

  if (theme == "PKMN 0") {
    // Draw the 16x13 bitmap to the right of the title text
    display.drawBitmap(titleWidth - 8, 0, bmpTitlePk0_A, 16, 14, SH110X_WHITE);

    // Draw the 8x5 bitmap repeatedly below the title text
    int bitmapWidth = 8;
    int numFullRepeats = (titleWidth - 8) / bitmapWidth;
    int remainder = titleWidth % bitmapWidth;
    for (int i = 0; i < numFullRepeats; i++) {
      display.drawBitmap(i * bitmapWidth, 9, bmpTitlePk0_B, bitmapWidth, 5, SH110X_WHITE);
    }
    // Draw a partial bitmap if necessary
    if (remainder > 0) {
      display.drawBitmap(numFullRepeats * bitmapWidth, 9, bmpTitlePk0_B, remainder, 5, SH110X_WHITE);
    }
  }

  display.setCursor(1, 1);
  if (theme != "PKMN 0") {                             // Shift the title down by 1 pixel{
    display.setTextColor(SH110X_BLACK, SH110X_WHITE);  // black text, white background
  }
  display.print(pages[currentPage]->title);          // This line prints the title of the current page
  display.setTextColor(SH110X_WHITE, SH110X_BLACK);  // white text, black background

  if (theme == "lines" || theme == "edge") {
    display.drawLine(0, 0, display.width() - 1, 0, SH110X_WHITE);  // Draw a line across the top of the screen
    display.drawLine(0, 0, 0, 13, SH110X_WHITE);                   // Draw a line on left
  }
  if (theme == "dots") {
    for (int i = 0; i < display.width(); i += 4) {
      display.drawPixel(i, 0, SH110X_WHITE);  // Draw a dot along the top of the screen
    }
    for (int i = 0; i < 8; i += 4) {
      display.drawPixel(0, i, SH110X_WHITE);  // Draw a dot along the left edge of the screen
    }
  }
  if (theme == "plain") {
    display.drawLine(0, 0, titleWidth, 0, SH110X_WHITE);  // Draw a line across the top of the screen
    display.drawLine(0, 0, 0, 8, SH110X_WHITE);           // Draw a line on left
  }
  if (theme == "lines") {
    display.drawLine(titleWidth, 8, display.width() - 1, 0, SH110X_WHITE);
    display.drawLine(titleWidth, 6, display.width() - 1, 0, SH110X_WHITE);
    display.drawLine(titleWidth, 3, display.width() - 1, 0, SH110X_WHITE);
  }
  if (theme == "round") {
    display.drawLine(0, 0, titleWidth, 0, SH110X_WHITE);  // Draw a line across the top of the screen
    display.drawLine(0, 0, 0, 8, SH110X_WHITE);           // Draw a line on left
    display.drawBitmap(titleWidth, 0, bmpTitleRound, 8, 9, SH110X_WHITE);
  }
  if (theme == "tri") {
    display.drawLine(0, 0, titleWidth, 0, SH110X_WHITE);  // Draw a line across the top of the screen
    display.drawLine(0, 0, 0, 8, SH110X_WHITE);           // Draw a line on left
    display.drawBitmap(titleWidth, 0, bmpTitleTri, 16, 9, SH110X_WHITE);
  }
  if (theme == "64") {
    display.drawLine(0, 0, titleWidth, 0, SH110X_WHITE);  // Draw a line across the top of the screen
    display.drawLine(0, 0, 0, 8, SH110X_WHITE);           // Draw a line on left
    display.drawBitmap(titleWidth, 0, bmpTitle64, 128, 9, SH110X_WHITE);
  }
  if (theme == "A") {
    display.drawLine(0, 0, titleWidth, 0, SH110X_WHITE);  // Draw a line across the top of the screen
    display.drawLine(0, 0, 0, 8, SH110X_WHITE);           // Draw a line on left
    display.drawBitmap(titleWidth, 0, bmpTitleA, 8, 9, SH110X_WHITE);
  }


  for (int i = 0; i < 16; i++) {
    int index = buttonOrder_a[i];
    int x = (i % 4) * 32;
    int y = 14 + (i / 4) * 13;

    display.setCursor(x + 2, y + 2);
    Button* button = pages[currentPage]->buttons[buttonOrder_a[i]];
    if (button != NULL) {
      if (currentPage == 0 && !numLockOn && button->altName != NULL) {
        display.print(button->altName);  // This line prints the buttons of the current page
      } else {
        display.print(button->name);  // This line prints the buttons of the current page
      }

      display.setCursor(x, y);
      if (theme == "dots") {
        for (int j = 0; j < 30; j += 4) {
          display.drawPixel(x + j, y, SH110X_WHITE);       // Draw a dot along the top of the button text
          display.drawPixel(x + j, y + 10, SH110X_WHITE);  // Draw a dot along the bottom of the button text
        }
        for (int j = 0; j < 30; j += 4) {
          display.drawPixel(x, y + j, SH110X_WHITE);       // Draw a dot along the left of the button text
          display.drawPixel(x + 28, y + j, SH110X_WHITE);  // Draw a dot along the right of the button text
        }
      }
    } else {
      // Draw an X over the button text
      display.drawLine(x + 1, y, x + 29, y + 10, SH110X_WHITE);  // Adjust the coordinates as needed
      display.drawLine(x + 29, y, x + 1, y + 10, SH110X_WHITE);  // Adjust the coordinates as needed
    }
  }
  display.display();
  //delay(10);
}

