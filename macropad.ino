#include <Arduino.h>
#include <Adafruit_SH110X.h>
#include <Adafruit_NeoPixel.h>
#include <RotaryEncoder.h>
#include <Wire.h>
#include "Adafruit_NeoKey_1x4.h"
#include "seesaw_neopixel.h"
#include "Adafruit_TinyUSB.h"
#include "Adafruit_USBD_CDC.h"
#include <EEPROM.h>

#include "page.h"
#include "A_UTOPIA_DEFAULT_WAV.h"
#include "A_ALARM_WAV.h"


const uint8_t audioSample[] PROGMEM = {
  // Your audio data goes here
};

#define SPEAKER_PIN 16
#define SPEAKER_SD_PIN 14


Adafruit_NeoKey_1x4 neokey;
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_NEOPIXEL, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ800);
Adafruit_SH1106G display = Adafruit_SH1106G(128, 64, &SPI1, OLED_DC, OLED_RST, OLED_CS);
//uint8_t snapshot[64][128];


RotaryEncoder encoder(PIN_ROTA, PIN_ROTB, RotaryEncoder::LatchMode::FOUR3);
void checkPosition() {
  encoder.tick();
}  // just call tick() to check the state.
int encoder_pos = 0;

uint8_t j = 0;
bool pageChanged = true;

int currentPage = 0;
bool rectOnScreen = false;
bool buttonPressed = false;
bool capsLockOn = false;
bool capsLockLast = false;
bool numLockOn = false;
bool numLockLast = false;
bool encoderPressed = false;

//Todo: Move this to menu //////////////////////////////////////////////////
// Create a structure to hold your settings
struct Settings {
  char magicFlag[9];
  bool invertScreen;
  char theme[12];
  int brightness;
  int volume;
  bool capsEffect;
  bool wipeData;
  char buttonEffect[8];
  int color1;
  int color1Lum;
  int color2;
  int color2Lum;
  int colorSpeed;
  int colorSpeedCaps;
  char tranOut[8];
  char tranIn[8];
  char tranMenuIn[8];
  char tranMenuBlur[8];
};
Settings settings;


String themes[] = {
  "lines",
  "edge",
  "plain",
  "dots",
  "round",
  "tri",
  "64",
  "A",
  "PKMN 0"
};
String tranOutList[] = {
  "slide",
  "pixel",
  "swirl",
  "swirlZ",
  "zoom",
  "Vlines",
  "HLines",
  "HVLines",
  "static",
  "static2",
  "none"
};
String tranInList[] = {
  "slide",
  "zoom",
  "none"

};
String menuInList[] = {
  "box",
  "none"
};
String menuBlurList[] = {
  "VLines",
  "HLines",
  "HVLines",
  "swirlZ",
  "swirl",
  "pixelate",
  "zoom",
  "slide",
  "static",
  "static2",
  "none"

};

int numThemes = sizeof(themes) / sizeof(themes[0]);
int themeIndex = 0;
int numtranOutList = sizeof(tranOutList) / sizeof(tranOutList[0]);
int tranOutListIndex = 0;
int numtranInList = sizeof(tranInList) / sizeof(tranInList[0]);
int tranInListIndex = 0;
int nummenuInList = sizeof(menuInList) / sizeof(menuInList[0]);
int menuInListIndex = 0;

// Settings
bool invertScreen = false;
String theme = "lines";
int brightness = 50;
int volume = 5;  // Range from 0 to 11
bool capsEffect = true;
bool wipeData = false;
String buttonEffect = "Flash";
int color1 = 57;
int color1Lum = 255;
int color2 = 191;
int color2Lum = 5;
int colorSpeed = 255;
int colorSpeedCaps = 4;
String tranOut = "zoom";
String tranIn = "zoom";
String tranMenuIn = "box";
String magicFlag = "Esoteric";
////////////////////////////////////////////////////////////////////////////////

//Program state
String mode = "MAIN";

//Move this to hotkeys/////////////////////////////////////////////////////////////////////
#define QUEUE_SIZE 1024
uint8_t keycode_queue[QUEUE_SIZE];
int queue_count = 0;

#define CC_QUEUE_SIZE 1024
uint16_t cc_queue[CC_QUEUE_SIZE];
int cc_queue_count = 0;

typedef void (*funcPtr)();
#define FP_QUEUE_SIZE 10
funcPtr funcQueue[QUEUE_SIZE];
int funcQueueCount = 0;
/////////////////////////////////////////////////////////////////////////////////////////////////////

//const int buttonOrder_a[16] = {0, 1, 2, 15, 3, 4, 5, 14, 6, 7, 8, 13, 9, 10, 11, 12};
const int buttonOrder_a[16] = { 0, 1, 2, 12, 3, 4, 5, 13, 6, 7, 8, 14, 9, 10, 11, 15 };
const int buttonOrder_b[16] = { 0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14, 15, 11, 7, 3 };
//const int buttonOrder_b[16] = {0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14, 3, 7, 11, 15 };
bool buttonState[16] = { false };

unsigned long lastButtonPressTime = 0;  // The time the last button was pressed

// HID report descriptor using TinyUSB's template
// Single Report (no ID) descriptor
uint8_t const desc_hid_report[] = {
  TUD_HID_REPORT_DESC_KEYBOARD(),
  TUD_HID_REPORT_DESC_CONSUMER()
};
// USB HID object. For ESP32 these values cannot be changed after this declaration
// desc report, desc len, protocol, interval, use out endpoint
Adafruit_USBD_HID usb_hid(desc_hid_report, sizeof(desc_hid_report), HID_ITF_PROTOCOL_KEYBOARD, 2, false);
// For keycode definition check out https://github.com/hathach/tinyusb/blob/master/src/class/hid/hid.h
uint8_t hidcode[] = {};



int oldPos = 0;  // Store the old position of encoder

//External variables
extern bool menuOpen;
extern bool menuEncMod;
extern const int numPages;
extern Page* pages[];
void enqueue_keycode(uint8_t keycode, uint8_t modifiers);
void enqueue_keycode(uint8_t keycode);
extern uint8_t debugKeycode;
extern uint16_t debugKeycodeCEC;




// //Framebuffer
// class FramebufferDisplay : public Adafruit_GFX {
// public:
//   FramebufferDisplay(uint8_t framebuffer[64][128]) : Adafruit_GFX(128, 64), framebuffer(framebuffer) {}

//   void drawPixel(int16_t x, int16_t y, uint16_t color) override {
//     if (x >= 0 && x < 128 && y >= 0 && y < 64) {
//       framebuffer[y][x] = color;
//     }
//   }

// private:
//   uint8_t (*framebuffer)[128];
// };

// uint8_t framebuffer[64][128];
// FramebufferDisplay display(framebuffer);

// // Create a framebuffer
// uint8_t framebuffer[64][128];

// // Function to draw the framebuffer on the screen
// void drawFramebuffer() {
//   for (int y = 0; y < 64; y++) {
//     for (int x = 0; x < 128; x++) {
//       if (framebuffer[y][x]) {
//         display.drawPixel(x, y, SH110X_WHITE);
//       } else {
//         display.drawPixel(x, y, SH110X_BLACK);
//       }
//     }
//   }
// }

// // Function to slide the framebuffer to the left
// void slideFramebufferLeft() {
//   for (int y = 0; y < 64; y++) {
//     for (int x = 0; x < 127; x++) {
//       framebuffer[y][x] = framebuffer[y][x + 1];
//     }
//     framebuffer[y][127] = 0;  // Clear the rightmost column
//   }
// }

// // Function to slide the framebuffer to the right
// void slideFramebufferRight() {
//   for (int y = 0; y < 64; y++) {
//     for (int x = 127; x > 0; x--) {
//       framebuffer[y][x] = framebuffer[y][x - 1];
//     }
//     framebuffer[y][0] = 0;  // Clear the leftmost column
//   }
// }

// // Function to save a snapshot of the framebuffer
// void saveFramebuffer(uint8_t snapshot[64][128]) {
//   for (int y = 0; y < 64; y++) {
//     for (int x = 0; x < 128; x++) {
//       snapshot[y][x] = framebuffer[y][x];
//     }
//   }
// }

// // Function to restore a snapshot of the framebuffer
// void restoreFramebuffer(uint8_t snapshot[64][128]) {
//   for (int y = 0; y < 64; y++) {
//     for (int x = 0; x < 128; x++) {
//       framebuffer[y][x] = snapshot[y][x];
//     }
//   }
// }


// Output report callback for LED indicator such as Caplocks
void hid_report_callback(uint8_t report_id, hid_report_type_t report_type, uint8_t const* buffer, uint16_t bufsize) {
  (void)report_id;
  (void)bufsize;

  // LED indicator is output report with only 1 byte length
  if (report_type != HID_REPORT_TYPE_OUTPUT) return;

  // The LED bit map is as follows: (also defined by KEYBOARD_LED_* )
  // Kana (4) | Compose (3) | ScrollLock (2) | CapsLock (1) | Numlock (0)
  uint8_t ledIndicator = buffer[0];

  // turn on LED if capslock is set
  //digitalWrite(LED_BUILTIN, ledIndicator & KEYBOARD_LED_CAPSLOCK);
  capsLockOn = ledIndicator & KEYBOARD_LED_CAPSLOCK;
  numLockOn = ledIndicator & KEYBOARD_LED_NUMLOCK;
  if (numLockOn != numLockLast) {
    //pageChanged = true;
    numLockLast = numLockOn;
  }
  if (capsLockOn != capsLockLast) {
    //pageChanged = true;
    capsLockLast = capsLockOn;
  }
}
void setup() {


  display.begin(0, true);  // we dont use the i2c address but we will reset!
  display.setTextSize(1);
  display.setTextWrap(false);
  display.setTextColor(SH110X_WHITE, SH110X_BLACK);  // white text, black background

  display.clearDisplay();
  display.setCursor(0, 0);
  display.print("ES Macropad");
  display.setCursor(0, 30);
  display.print("Loading...");
  display.display();

  // Manual begin() is required on core without built-in support for TinyUSB such as mbed rp2040
  //TinyUSB_Device_Init(0);
  SerialTinyUSB.begin(115200);
  //delay(1500);  // RP2040 delay is not a bad idea, uncomment if early boot issues

  pinMode(SPEAKER_SD_PIN, OUTPUT);
  digitalWrite(SPEAKER_SD_PIN, HIGH);  // Turn on the amplifier

  digitalWrite(SPEAKER_SD_PIN, LOW);  // Turn off the amplifier

  pinMode(SPEAKER_PIN, OUTPUT);
  SerialTinyUSB.println("Adafruit Macropad RP2040 with NeoKey 1x4");

  SerialTinyUSB.println("Accessing flash...");

  // Settings settings;
  noInterrupts();

  // Load the settings from EEPROM
  EEPROM.begin(sizeof(settings));
  EEPROM.get(0, settings);
  EEPROM.end();

  interrupts();
  // Convert the character arrays back to String objects
  settings.magicFlag[sizeof(settings.magicFlag) - 1] = '\0';        // Ensure null termination
  settings.theme[sizeof(settings.theme) - 1] = '\0';                // Ensure null termination
  settings.buttonEffect[sizeof(settings.buttonEffect) - 1] = '\0';  // Ensure null termination
  settings.tranOut[sizeof(settings.tranOut) - 1] = '\0';            // Ensure null termination
  settings.tranIn[sizeof(settings.tranIn) - 1] = '\0';              // Ensure null termination
  settings.tranMenuIn[sizeof(settings.tranMenuIn) - 1] = '\0';      // Ensure null termination
  String s_magicFlag = String(settings.magicFlag);
  SerialTinyUSB.println("s_magicFlag: [" + s_magicFlag + "]");
  if (s_magicFlag == magicFlag) {
    // Use the loaded settings
    invertScreen = settings.invertScreen;
    brightness = settings.brightness;
    volume = settings.volume;
    capsEffect = settings.capsEffect;
    wipeData = settings.wipeData;
    color1 = settings.color1;
    color1Lum = settings.color1Lum;
    color2 = settings.color2;
    color2Lum = settings.color2Lum;
    colorSpeed = settings.colorSpeed;
    colorSpeedCaps = settings.colorSpeedCaps;
    theme = String(settings.theme);
    buttonEffect = String(settings.buttonEffect);
    tranOut = String(settings.tranOut);
    tranIn = String(settings.tranIn);
    tranMenuIn = String(settings.tranMenuIn);


    int thisIndex = -1;  // Start with an invalid index
    for (int i = 0; i < numThemes; i++) {
      if (themes[i] == theme) {
        thisIndex = i;
        break;  // Exit the loop once the theme is found
      }
    }
    if (thisIndex > -1) {
      themeIndex = thisIndex;
    } else {
      themeIndex = 0;
      theme = themes[0];
    }

    thisIndex = -1;  // Start with an invalid index
    for (int i = 0; i < numtranOutList; i++) {
      if (tranOutList[i] == tranOut) {
        thisIndex = i;
        break;  // Exit the loop once the theme is found
      }
    }
    if (thisIndex > -1) {
      tranOutListIndex = thisIndex;
    } else {
      tranOutListIndex = 0;
      tranOut = tranOutList[0];
    }

    thisIndex = -1;  // Start with an invalid index
    for (int i = 0; i < numtranInList; i++) {
      if (tranInList[i] == tranIn) {
        thisIndex = i;
        break;  // Exit the loop once the theme is found
      }
    }
    if (thisIndex > -1) {
      tranInListIndex = thisIndex;
    } else {
      tranInListIndex = 0;
      tranIn = tranInList[0];
    }

    thisIndex = -1;  // Start with an invalid index
    for (int i = 0; i < nummenuInList; i++) {
      if (menuInList[i] == tranMenuIn) {
        thisIndex = i;
        break;  // Exit the loop once the theme is found
      }
    }
    if (thisIndex > -1) {
      menuInListIndex = thisIndex;
    } else {
      menuInListIndex = 0;
      tranMenuIn = menuInList[0];
    }

    // SerialTinyUSB.print("invertScreen: ");
    // SerialTinyUSB.println(settings.invertScreen);
    // SerialTinyUSB.print("theme: ");
    // SerialTinyUSB.println(settings.theme);
    // SerialTinyUSB.print("brightness: ");
    // SerialTinyUSB.println(settings.brightness);
    // SerialTinyUSB.print("volume: ");
    // SerialTinyUSB.println(settings.volume);
    // SerialTinyUSB.print("capsEffect: ");
    // SerialTinyUSB.println(settings.capsEffect);
    // SerialTinyUSB.print("wipeData: ");
    // SerialTinyUSB.println(settings.wipeData);
    // SerialTinyUSB.print("buttonEffect: ");
    // SerialTinyUSB.println(settings.buttonEffect);
    // SerialTinyUSB.print("color1: ");
    // SerialTinyUSB.println(settings.color1);
    // SerialTinyUSB.print("color1Lum: ");
    // SerialTinyUSB.println(settings.color1Lum);
    // SerialTinyUSB.print("color2: ");
    // SerialTinyUSB.println(settings.color2);
    // SerialTinyUSB.print("color2Lum: ");
    // SerialTinyUSB.println(settings.color2Lum);
    // SerialTinyUSB.print("colorSpeed: ");
    // SerialTinyUSB.println(settings.colorSpeed);
    // SerialTinyUSB.print("colorSpeedCaps: ");
    // SerialTinyUSB.println(settings.colorSpeedCaps);
  } else {
    SerialTinyUSB.println("Flash doesn't contain valid save data...");
    // SerialTinyUSB.print("invertScreen: ");
    // SerialTinyUSB.println(settings.invertScreen);
    // SerialTinyUSB.print("theme: ");
    // SerialTinyUSB.println(settings.theme);
    // SerialTinyUSB.print("brightness: ");
    // SerialTinyUSB.println(settings.brightness);
    // SerialTinyUSB.print("volume: ");
    // SerialTinyUSB.println(settings.volume);
    // SerialTinyUSB.print("capsEffect: ");
    // SerialTinyUSB.println(settings.capsEffect);
    // SerialTinyUSB.print("wipeData: ");
    // SerialTinyUSB.println(settings.wipeData);
    // SerialTinyUSB.print("buttonEffect: ");
    // SerialTinyUSB.println(settings.buttonEffect);
    // SerialTinyUSB.print("color1: ");
    // SerialTinyUSB.println(settings.color1);
    // SerialTinyUSB.print("color1Lum: ");
    // SerialTinyUSB.println(settings.color1Lum);
    // SerialTinyUSB.print("color2: ");
    // SerialTinyUSB.println(settings.color2);
    // SerialTinyUSB.print("color2Lum: ");
    // SerialTinyUSB.println(settings.color2Lum);
    // SerialTinyUSB.print("colorSpeed: ");
    // SerialTinyUSB.println(settings.colorSpeed);
    // SerialTinyUSB.print("colorSpeedCaps: ");
    // SerialTinyUSB.println(settings.colorSpeedCaps);

    EEPROM.begin(127);
  }
  pixels.begin();
  pixels.setBrightness(255);
  pixels.show();  // Initialize all pixels to 'off'
  // //display.display();
  // delay(500);

  for (uint8_t i = 0; i <= 12; i++) {
    pinMode(i, INPUT_PULLUP);
  }

  pinMode(PIN_ROTA, INPUT_PULLUP);
  pinMode(PIN_ROTB, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIN_ROTA), checkPosition, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_ROTB), checkPosition, CHANGE);

  Wire.begin();

  display.invertDisplay(invertScreen);  // Invert the display colors

  if (!neokey.begin(0x30)) {
    SerialTinyUSB.println("Cannot find Neokey 1x4 at 0x30, check wiring?");
    display.clearDisplay();
    display.setCursor(0, 0);
    display.print("Could not detect ");
    display.setCursor(0, 10);
    display.print("NeoKey 1x4 on i2c ");
    display.setCursor(0, 20);
    display.print("0x30. Check wiring.");
    display.setCursor(0, 30);
    display.print("Can not continue...");
    display.display();
    while (1)
      ;
  }
  setupPages();

  SerialTinyUSB.println("Starting keyboard");
}

void setup1() {
  // Notes: following commented-out functions has no affect on ESP32
  usb_hid.setBootProtocol(HID_ITF_PROTOCOL_KEYBOARD);
  usb_hid.setPollInterval(2);
  usb_hid.setReportDescriptor(desc_hid_report, sizeof(desc_hid_report));
  usb_hid.setStringDescriptor("TinyUSB Keyboard");

  // Set up output report (on control endpoint) for Capslock indicator
  usb_hid.setReportCallback(NULL, hid_report_callback);


  if (usb_hid.begin()) {
    SerialTinyUSB.println("USB HID started successfully");
  } else {
    SerialTinyUSB.println("Failed to start USB HID");
  }
  SerialTinyUSB.println("Setup complete");
  SerialTinyUSB.println("Setup1 complete");
}
// // Running on second core
// void setup1() {
//   delay(5000);
//   SerialTinyUSB.printf("C1: Red leader standing by...\n");
// }

// void loop1() {
//   SerialTinyUSB.printf("C1: Stay on target...\n");
//   delay(500);
// }



void loop() {

  // static unsigned long lastDrawTime = 0;

  // // Check if it's time to draw a frame
  // if (millis() - lastDrawTime >= 1000 / 30) {
  //   // Draw the framebuffer on the screen
  //   for (int y = 0; y < 64; y++) {
  //     for (int x = 0; x < 128; x++) {
  //       if (framebuffer[y][x]) {
  //         display.drawPixel(x, y, SH110X_WHITE);
  //       } else {
  //         display.drawPixel(x, y, SH110X_BLACK);
  //       }
  //     }
  //   }

  //   // Update the display
  //   display.display();

  //   // Update the draw time
  //   lastDrawTime = millis();
  // }

  if (pageChanged && !menuOpen) {
    SerialTinyUSB.println("PageChange");
    //slide_out();
    display.clearDisplay();

    display.setContrast(0);

    drawPage();
    //delay(20);
    //

    transitionIn();
    // if (tranIn == "slide") {
    //   slide_in();
    //   drawPage();
    // } else if (tranIn == "zoom") {
    //   grow_in();
    //   drawPage();
    // }
    pageChanged = false;
  } else if (rectOnScreen && !menuOpen) {
    display.clearDisplay();
    drawPage();
    //delay(20);
    //
    rectOnScreen = false;
  }
  encoder.tick();
  int newPos = encoder.getPosition();
  int diff = abs(newPos - oldPos);  // Calculate the difference

  if (diff > 0) {  // Check if the position has changed
    SerialTinyUSB.println("Encoder Change");
    for (int i = 0; i < diff; i++) {  // Loop for the difference
      if (menuOpen) {
        if (menuEncMod) {
          if (newPos < oldPos) {
            menuDown();
          } else {
            menuUp();
          }
        } else {
          if (newPos > oldPos) {
            valueDown();
          } else {
            valueUp();
          }
        }
      } else {
        currentPage = newPos % numPages;
        if (currentPage < 0) {
          currentPage += numPages;  // Wrap around to the last page
        }
        pageChanged = true;
        buttonPressed = true;
        transitionOut();
        // if (tranOut == "slide") {
        //   slide_out();
        // } else if (tranOut == "pixel") {
        //   pixelate_out();
        // } else if (tranOut == "swirl") {
        //   swirl_out();
        // } else if (tranOut == "swirlZ") {
        //   swirl_scale_out();
        // } else if (tranOut == "zoom") {
        //   shrink_out();
        // } else if (tranOut == "Hlines") {
        //   fillScreenRandomly();
        // }
      }
    }
    oldPos = newPos;  // Update the old position
  }

  if (!digitalRead(PIN_SWITCH)) {
    if (!encoderPressed) {
      SerialTinyUSB.println("Encoder button");
      menuToggle();
      encoderPressed = true;
    }
    buttonPressed = true;
  } else {
    encoderPressed = false;
  }

  static unsigned long repeatTimer = 0;
  static int repeatButton = -1;

  for (uint8_t i = 0; i < 12; i++) {
    bool isPressed = !digitalRead(i + 1);  // switch pressed!
    Button* button = pages[currentPage]->buttons[i];

    int x = (buttonOrder_b[i] % 4) * 32;
    int y = 14 + (buttonOrder_b[i] / 4) * 13;
    display.setCursor(x + 2, y + 2);
    if (isPressed && !menuOpen) {
      if (buttonEffect != "Off") {
        rectOnScreen = true;
        if (buttonEffect == "Dots") {
          // Draw a rectangle of dots around the button text
          for (int j = 0; j < 30; j += 4) {
            display.drawPixel(x + j, y, SH110X_WHITE);       // Draw a dot along the top of the button text
            display.drawPixel(x + j, y + 10, SH110X_WHITE);  // Draw a dot along the bottom of the button text
          }
          for (int j = 0; j < 10; j += 4) {
            display.drawPixel(x, y + j, SH110X_WHITE);       // Draw a dot along the left of the button text
            display.drawPixel(x + 30, y + j, SH110X_WHITE);  // Draw a dot along the right of the button text
          }
        } else if (buttonEffect == "Solid") {
          // Draw a solid rectangle around the button text
          display.drawRect(x, y, 31, 11, SH110X_WHITE);
        } else if (buttonEffect == "Flash") {
          // Alternate between a rectangle of dots and a solid rectangle
          if (millis() % 500 < 250) {
            display.drawRect(x, y, 31, 11, SH110X_WHITE);
          } else {
            display.drawRect(x, y, 31, 11, SH110X_BLACK);
            for (int j = 0; j < 30; j += 4) {
              display.drawPixel(x + j, y, SH110X_WHITE);       // Draw a dot along the top of the button text
              display.drawPixel(x + j, y + 10, SH110X_WHITE);  // Draw a dot along the bottom of the button text
            }
            for (int j = 0; j < 10; j += 4) {
              display.drawPixel(x, y + j, SH110X_WHITE);       // Draw a dot along the left of the button text
              display.drawPixel(x + 30, y + j, SH110X_WHITE);  // Draw a dot along the right of the button text
            }
          }
        }
      }
    }
    if (isPressed != buttonState[i]) {
      // Button state has changed
      if (isPressed && !buttonState[i]) {  // Button is pressed and was not pressed before
        SerialTinyUSB.println("Switch " + String(i));
        if (!menuOpen) {
          if (button != NULL) {
            hotkeys(button->param);
          }
        } else {
          if (i == 0) {
            menuEncMod = !menuEncMod;
            drawMenu();
          } else if (i == 4) {
            menuUp();
          } else if (i == 6) {
            valueDown();
          } else if (i == 8) {
            valueUp();
          } else if (i == 10) {
            menuDown();
          } else if (i == 9) {
            enqueue_keycode(debugKeycode);

          } else if (i == 11) {
            enqueue_cc_code(debugKeycodeCEC);
          }
        }
      }
      buttonState[i] = isPressed;
      buttonPressed = true;
    }
  }

  uint8_t buttons = neokey.read();
  for (uint8_t i = 0; i < 4; i++) {
    bool isPressed = buttons & (1 << i);
    Button* button = pages[currentPage]->buttons[(3 - i) + 12];

    if (buttonEffect != "Off") {
      int x = (buttonOrder_b[i + 12] % 4) * 32;
      int y = 14 + (buttonOrder_b[i + 12] / 4) * 13;
      display.setCursor(x + 2, y + 2);
      // if (currentPage == 0 && !numLockOn && button->altName != NULL) {
      //   display.print(button->altName);  // This line prints the buttons of the current page
      // } else {
      //   display.print(button->name);  // This line prints the buttons of the current page
      // }

      if (isPressed && !menuOpen) {
        if (buttonEffect != "Off") {
          rectOnScreen = true;
          if (buttonEffect == "Dots") {
            // Draw a rectangle of dots around the button text
            for (int j = 0; j < 30; j += 4) {
              display.drawPixel(x + j, y, SH110X_WHITE);       // Draw a dot along the top of the button text
              display.drawPixel(x + j, y + 10, SH110X_WHITE);  // Draw a dot along the bottom of the button text
            }
            for (int j = 0; j < 10; j += 4) {
              display.drawPixel(x, y + j, SH110X_WHITE);       // Draw a dot along the left of the button text
              display.drawPixel(x + 30, y + j, SH110X_WHITE);  // Draw a dot along the right of the button text
            }
          } else if (buttonEffect == "Solid") {
            // Draw a solid rectangle around the button text
            display.drawRect(x, y, 31, 11, SH110X_WHITE);
          } else if (buttonEffect == "Flash") {
            // Alternate between a rectangle of dots and a solid rectangle
            if (millis() % 500 < 250) {
              display.drawRect(x, y, 31, 11, SH110X_WHITE);
            } else {
              display.drawRect(x, y, 31, 11, SH110X_BLACK);
              for (int j = 0; j < 30; j += 4) {
                display.drawPixel(x + j, y, SH110X_WHITE);       // Draw a dot along the top of the button text
                display.drawPixel(x + j, y + 10, SH110X_WHITE);  // Draw a dot along the bottom of the button text
              }
              for (int j = 0; j < 10; j += 4) {
                display.drawPixel(x, y + j, SH110X_WHITE);       // Draw a dot along the left of the button text
                display.drawPixel(x + 30, y + j, SH110X_WHITE);  // Draw a dot along the right of the button text
              }
            }
          }
        }
      }
    }
    //display.display();
    if (isPressed != buttonState[12 + i]) {
      // Button state has changed
      if (isPressed && !buttonState[12 + i]) {  // Button is pressed and was not pressed before
        SerialTinyUSB.println("Neokey " + String(i));
        if (!menuOpen) {
          if (button != NULL) {
            hotkeys(button->param);
          }
        }
      }
      buttonState[12 + i] = isPressed;
      buttonPressed = true;
    }
  }


  int speed = 1;
  display.setCursor(90, 1);
  if (capsLockOn) {
    display.print("CL");
    if (capsEffect) {
      pixels.setBrightness(int(brightness * 2.5));
      neokey.pixels.setBrightness(int((brightness + 1) * 2.5));
      speed = colorSpeedCaps;
    }
  } else {
    if (capsEffect) {
      pixels.setBrightness(int(brightness * 1.6));
      neokey.pixels.setBrightness(int((brightness + 1) * 1.6));
    } else {
      pixels.setBrightness(int(brightness * 2.5));
      neokey.pixels.setBrightness(int((brightness + 1) * 2.5));
    }
    speed = colorSpeed;
    //   display.print("  ");
  }

  display.setCursor(105, 1);
  if (numLockOn && !menuOpen) {
    display.print("NL");
  }
  // else {
  //   display.print("  ");
  // }



  j++;
  for (int i = 0; i < pixels.numPixels(); i++) {
    pixels.setPixelColor(i, ColorWheel(((i * 256 / pixels.numPixels()) + (j * speed)) & 255, color1, color2, color1Lum, color2Lum));
    // pixels.setPixelColor(i, BlueGreenWheel(((i * 256 / pixels.numPixels()) + (j * speed)) & 255));
  }
  for (int i = 0; i < neokey.pixels.numPixels(); i++) {
    neokey.pixels.setPixelColor(3 - i, ColorWheel((((i * 256) / neokey.pixels.numPixels()) + (j * speed) + 63) & 255, color1, color2, color1Lum, color2Lum));
    // neokey.pixels.setPixelColor(3 - i, BlueGreenWheel((((i * 256) / neokey.pixels.numPixels()) + (j * speed) + 64) & 255));
  }

  pixels.show();

  neokey.pixels.show();

  if (rectOnScreen || capsLockOn || numLockOn) {
    display.display();
  }
  //delay(10);
  //display.display();
}


void loop1() {
  // poll gpio no more than each 2 ms
  delay(2);


  // If there are functions in the queue
  if (funcQueueCount > 0) {
    // Call the first function in the queue
    funcQueue[0]();

    // Shift all items in the queue to the left
    for (int i = 0; i < funcQueueCount - 1; i++) {
      funcQueue[i] = funcQueue[i + 1];
    }
    funcQueueCount--;
  }

  // for (int i = 0; i < 16; i++) {
  //   if (buttonPressed) {
  //     lastButtonPressTime = millis(); // Update the last button press time
  //     buttonPressed = false;

  //     //display.begin(0, true);
  //     // Cancel the screensaver if it's running
  //   }
  // }

  // // If no button has been pressed for 10 seconds, start the screensaver
  // if (millis() - lastButtonPressTime > 10000) {
  //   randomPixelScreensaver();
  // }

  // If there are keycodes in the queue and the HID device is ready
  if (TinyUSBDevice.suspended() && queue_count) {
    // Wake up host if we are in suspend mode
    // and REMOTE_WAKEUP feature is enabled by host
    TinyUSBDevice.remoteWakeup();
  }

  // skip if hid is not ready e.g still transferring previous report
  if (!usb_hid.ready()) return;



  if (queue_count > 0 && usb_hid.ready()) {
    uint8_t modifier = 0;
    uint8_t keycode[6] = { 0 };  // Array of keycodes
    uint8_t count = 0;

    // Dequeue up to 2 modifier-keycode pairs and put them in the modifier and keycode array
    while (count < 2 && queue_count >= 2) {  // Make sure there's a modifier and keycode to dequeue
      modifier = keycode_queue[0];
      keycode[count] = keycode_queue[1];  // Put the keycode in the keycode array
      // Shift all items in the queue to the left
      for (int i = 0; i < queue_count - 2; i++) {
        keycode_queue[i] = keycode_queue[i + 2];
      }
      queue_count -= 2;
      count++;

      // Send the modifier and keycode
      uint8_t const report_id = 0;
      usb_hid.keyboardReport(report_id, modifier, keycode);

      delay(10);

      // Release all keys
      usb_hid.keyboardRelease(0);
    }
  }

  if (cc_queue_count > 0 && usb_hid.ready()) {
    uint16_t cc_code = cc_queue[0];
    // Shift all items in the queue to the left
    for (int i = 0; i < cc_queue_count - 1; i++) {
      cc_queue[i] = cc_queue[i + 1];
    }
    cc_queue_count--;

    // Send the Consumer Control code
    usb_hid.sendReport(HID_USAGE_CONSUMER_CONTROL, &cc_code, 2);
  }
}