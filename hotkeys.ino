


void hotkeys(int param) {
  SerialTinyUSB.print("Hotkey: ");
  SerialTinyUSB.println(param);

  uint8_t keycode = 0;

  if (param == 0) {
    enqueue_keycode(HID_KEY_A);
  } else if (param == 1) {
    enqueue_keycode(HID_KEY_B);
  } else if (param == 2) {
    enqueue_keycode(HID_KEY_C);
  } else if (param == 3) {
    enqueue_keycode(HID_KEY_D);
  } else if (param == 4) {
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_MINUS);
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_ENTER);
  } else if (param == 5) {
    enqueue_keycode(HID_KEY_X, KEYBOARD_MODIFIER_LEFTGUI);
    delay(600);
    enqueue_keycode(HID_KEY_A);
    delay(600);
    enqueue_keycode(HID_KEY_Y, KEYBOARD_MODIFIER_LEFTALT);
    delay(4000);
    enqueue_keycode(HID_KEY_D);
    enqueue_keycode(HID_KEY_I);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_M);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_SLASH);
    enqueue_keycode(HID_KEY_O);
    enqueue_keycode(HID_KEY_N);
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_I);
    enqueue_keycode(HID_KEY_N);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_SLASH);
    enqueue_keycode(HID_KEY_C);
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_N);
    enqueue_keycode(HID_KEY_U);
    enqueue_keycode(HID_KEY_P);
    enqueue_keycode(HID_KEY_MINUS);
    enqueue_keycode(HID_KEY_I);
    enqueue_keycode(HID_KEY_M);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_G);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_SLASH);
    enqueue_keycode(HID_KEY_R);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_T);
    enqueue_keycode(HID_KEY_O);
    enqueue_keycode(HID_KEY_R);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_H);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_T);
    enqueue_keycode(HID_KEY_H);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_SEMICOLON);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_F);
    enqueue_keycode(HID_KEY_C);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_SLASH);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_C);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_N);
    enqueue_keycode(HID_KEY_N);
    enqueue_keycode(HID_KEY_O);
    enqueue_keycode(HID_KEY_W);
    enqueue_keycode(HID_KEY_ENTER);
  }  else if (param == 8) {
    enqueue_cc_code(HID_USAGE_CONSUMER_VOLUME_DECREMENT);
  } else if (param == 9) {
    enqueue_cc_code(HID_USAGE_CONSUMER_VOLUME_INCREMENT);
  } else if (param == 10) {
    enqueue_cc_code(HID_USAGE_CONSUMER_MUTE);
  } else if (param == 12) {
    enqueue_keycode(HID_KEY_COPY);
  } else if (param == 13) {
    enqueue_keycode(HID_KEY_CUT);
  } else if (param == 14) {
    enqueue_keycode(HID_KEY_PASTE);
  }

  else if (param == 15) {
    enqueue_keycode(HID_KEY_SEPARATOR);
  } else if (param == 90) {
    enqueue_keycode(HID_KEY_KEYPAD_0);
  } else if (param == 91) {
    enqueue_keycode(HID_KEY_KEYPAD_1);
  } else if (param == 92) {
    enqueue_keycode(HID_KEY_KEYPAD_2);
  } else if (param == 93) {
    enqueue_keycode(HID_KEY_KEYPAD_3);
  } else if (param == 94) {
    enqueue_keycode(HID_KEY_KEYPAD_4);
  } else if (param == 95) {
    enqueue_keycode(HID_KEY_KEYPAD_5);
  } else if (param == 96) {
    enqueue_keycode(HID_KEY_KEYPAD_6);
  } else if (param == 97) {
    enqueue_keycode(HID_KEY_KEYPAD_7);
  } else if (param == 98) {
    enqueue_keycode(HID_KEY_KEYPAD_8);
  } else if (param == 99) {
    enqueue_keycode(HID_KEY_KEYPAD_9);
  } else if (param == 100) {
    enqueue_keycode(HID_KEY_NUM_LOCK);
  } else if (param == 101) {
    enqueue_keycode(HID_KEY_KEYPAD_DIVIDE);
  } else if (param == 102) {
    enqueue_keycode(HID_KEY_KEYPAD_MULTIPLY);
  } else if (param == 103) {
    enqueue_keycode(HID_KEY_KEYPAD_SUBTRACT);
  } else if (param == 104) {
    enqueue_keycode(HID_KEY_KEYPAD_ADD);
  } else if (param == 105) {
    enqueue_keycode(HID_KEY_KEYPAD_ENTER);
  } else if (param == 106) {
    enqueue_keycode(HID_KEY_KEYPAD_DECIMAL);
  }

  else if (param == 107) {               //Beep test
    digitalWrite(SPEAKER_SD_PIN, HIGH);  // Turn on the amplifier
    delay(50);
    tone(SPEAKER_PIN, 440);
    delay(50);

    // Stop the tone
    noTone(SPEAKER_PIN);

    digitalWrite(SPEAKER_SD_PIN, LOW);   // Turn off the amplifier
  } else if (param == 108) {             //PCM test
    digitalWrite(SPEAKER_SD_PIN, HIGH);  // Turn on the amplifier
    delay(50);
    for (unsigned int i = 0; i < Utopia_Default_wav_len; i++) {
      uint32_t sample = Utopia_Default_wav[i];
      analogWrite(SPEAKER_PIN, sample);
      delayMicroseconds(125);  // For a sample rate of 8000 Hz
    }
    delay(50);
    digitalWrite(SPEAKER_SD_PIN, LOW);  // Turn off the amplifier
  }

  else if (param == 109) {  //spotlight mac
    enqueue_keycode(HID_KEY_SPACE, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 110) {  //close mac
    enqueue_keycode(HID_KEY_W, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 111) {  //quit mac
    enqueue_keycode(HID_KEY_Q, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 112) {  //cut mac
    enqueue_keycode(HID_KEY_X, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 113) {  //copy mac
    enqueue_keycode(HID_KEY_C, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 114) {  //paste mac
    enqueue_keycode(HID_KEY_V, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 115) {  //new mac
    enqueue_keycode(HID_KEY_N, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 116) {  //save mac
    enqueue_keycode(HID_KEY_S, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 117) {  //say mac
    enqueue_keycode(HID_KEY_SPACE, KEYBOARD_MODIFIER_LEFTGUI);
    delay(200);
    enqueue_keycode(HID_KEY_T);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_R);
    enqueue_keycode(HID_KEY_M);
    enqueue_keycode(HID_KEY_I);
    enqueue_keycode(HID_KEY_N);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_ENTER);
    delay(500);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_Y);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_APOSTROPHE, KEYBOARD_MODIFIER_LEFTSHIFT);
    enqueue_keycode(HID_KEY_C);
    enqueue_keycode(HID_KEY_H);
    enqueue_keycode(HID_KEY_R);
    enqueue_keycode(HID_KEY_I);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_A);
    enqueue_keycode(HID_KEY_Y);
    enqueue_keycode(HID_KEY_S);
    enqueue_keycode(HID_KEY_SPACE);
    enqueue_keycode(HID_KEY_H);
    enqueue_keycode(HID_KEY_I);
    enqueue_keycode(HID_KEY_APOSTROPHE, KEYBOARD_MODIFIER_LEFTSHIFT);
    enqueue_keycode(HID_KEY_ENTER);
    delay(2000);
    enqueue_keycode(HID_KEY_Q, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 118) {             //PCM test
    digitalWrite(SPEAKER_SD_PIN, HIGH);  // Turn on the amplifier
    delay(50);
    for (unsigned int i = 0; i < Alarm_wav_len; i++) {
      uint32_t sample = Alarm_wav[i];
      analogWrite(SPEAKER_PIN, sample);
      delayMicroseconds(125);  // For a sample rate of 8000 Hz
    }
    delay(50);
    digitalWrite(SPEAKER_SD_PIN, LOW);  // Turn off the amplifier
  } else if (param == 119) {
    enqueue_keycode(HID_KEY_S, KEYBOARD_MODIFIER_LEFTSHIFT);
    enqueue_keycode(HID_KEY_T);
    enqueue_keycode(HID_KEY_Y);
    enqueue_keycode(HID_KEY_L);
    enqueue_keycode(HID_KEY_E);
    enqueue_keycode(HID_KEY_2);
    enqueue_keycode(HID_KEY_0);
    enqueue_keycode(HID_KEY_2);
    enqueue_keycode(HID_KEY_2);
    enqueue_keycode(HID_KEY_1, KEYBOARD_MODIFIER_LEFTSHIFT);
    enqueue_keycode(HID_KEY_ENTER);

  } else if (param == 120) {
    mode = "BREAKOUT";
    // Add someFunction to the queue
    if (funcQueueCount < FP_QUEUE_SIZE) {
      funcQueue[funcQueueCount] = breakoutLoopB;
      funcQueueCount++;
    }
    breakoutLoopA();
  } else if (param == 121) {  //Cast Windows
    enqueue_keycode(HID_KEY_K, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 122) {  //Lock Windows
    enqueue_keycode(HID_KEY_L, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 123) {  //Present Windows
    enqueue_keycode(HID_KEY_P, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 124) {  //Run Windows
    enqueue_keycode(HID_KEY_R, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 125) {  //Emoji Windows
    enqueue_keycode(HID_KEY_SEMICOLON, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 126) {  //Shutdown Windows
    enqueue_keycode(HID_KEY_F16, KEYBOARD_MODIFIER_LEFTGUI);
  } else if (param == 127) {  //Clipboard Windows
    enqueue_keycode(HID_KEY_V, KEYBOARD_MODIFIER_LEFTGUI | KEYBOARD_MODIFIER_LEFTSHIFT);
  } else if (param == 128) {  //Touchpad Windows
    enqueue_keycode(HID_KEY_I, KEYBOARD_MODIFIER_LEFTGUI | KEYBOARD_MODIFIER_LEFTCTRL);
  } else if (param == 129) {
    mode = "LIGHTSOUT";
    // Add someFunction to the queue
    if (funcQueueCount < FP_QUEUE_SIZE) {
      funcQueue[funcQueueCount] = lightsoutLoopB;
      funcQueueCount++;
    }
    lightsoutLoopA();
  
  }
   else if (param == 130) {
    mode = "BABYGOAT";
    // Add someFunction to the queue
    if (funcQueueCount < FP_QUEUE_SIZE) {
      funcQueue[funcQueueCount] = bg_gameLoopB;
      funcQueueCount++;
    }
    bg_gameLoopA();
  
  }
  

  //  else if (param == 131) {
  //   mode = "YLYL";
  //   // Add someFunction to the queue
  //   if (funcQueueCount < FP_QUEUE_SIZE) {
  //     funcQueue[funcQueueCount] = yl_loopB;
  //     funcQueueCount++;
  //   }
  //   yl_loopA();
  
  // }
  

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  else if (param == 999) {
    mode = "TEST";
    // Add someFunction to the queue
    if (funcQueueCount < FP_QUEUE_SIZE) {
      funcQueue[funcQueueCount] = testLoopB;
      funcQueueCount++;
    }
    testLoopA();
  } else {
    display.setCursor(0, 0);
    display.print("Undef action " + String(param));
    display.display();
    delay(200);
    enqueue_keycode(HID_KEY_NONE);
  }

  return;
}



void enqueue_keycode(uint8_t keycode) {
    enqueue_keycode(keycode, 0);
}

void enqueue_keycode(uint8_t keycode, uint8_t modifiers) {
  if (queue_count < QUEUE_SIZE - 3) {  // Make sure there's room for the modifiers, keycode, and key release
    keycode_queue[queue_count++] = modifiers;
    keycode_queue[queue_count++] = keycode;
    keycode_queue[queue_count++] = 0;  // No modifiers for key release
    keycode_queue[queue_count++] = 0;  // Keycode of 0 for key release
  }
}

void enqueue_cc_code(uint16_t cc_code) {
  if (cc_queue_count < CC_QUEUE_SIZE) {
    cc_queue[cc_queue_count++] = cc_code;
  }
}

