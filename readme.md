# Esoteric 16 Key Adafruit MacroPad 
This project is a work-in-progress enhancement of the Adafruit MacroPad RP2040 running on Arduino C++. The enhancement includes an Adafruit 1x4 NeoKey, connected on the right of the main keys with a Quiic cable, bringing the total keys to 16. The project includes predefined pages and hotkeys, transition effects, a settings menu, and sample games, and takes advantage of the second core of the RP2040. 

Seriously, this is a work in progress and as of now doesn't properly work on all Windows machines, for some reason... This also replaces CircuitPython, which came preinstalled on the device, and you will most likely lose some/all code and data stored on the circuitpy drive if you install this. You can always flash CircuitPython again, but the code may not still be there, or may be corrupt. BACKUP CODE YOU CARE ABOUT BEFORE INSTALLING THIS!

You will need the 1x4 neokey, I haven't figured out the best way to handle switching between 12 and 16 key pages cleanly, so it's required. I will likely be reworking the page and hotkey logic once I work out the best way to handle 16 hotkeys on 12 keys. Might try out a sliding thing, but it's easier to just not support 12 keys for now. Currently, the neokey cannot be hotplugged and must be connected at boot, so it's Nintendo 64 perephrial rules until I write hotplug code. 

I also mounted my personal 1x4 neokey upside down, so the code is written to deal with that, I will be correcting this soon with an option for logically flipping the neokey, so it won't matter how you install it. 


## Features
16-Key MacroPad: Includes an additional Adafruit 1x4 NeoKey for a total of 16 keys.  
Predefined Pages and Hotkeys: Allows for custom hotkey configurations across multiple pages.  
Rotary Knob Control: Turning the rotary knob changes pages of hotkeys.  
Settings Menu: Opened with the rotary button, includes various customization options like screen inversion, theme selection, NeoPixel brightness, volume, caps lock effect, button effect, and more.  
Transition Effects: Customizable effects for opening/closing menus and changing pages.  
Sample Games: Includes sample games that utilize both cores of the RP2040.  

## Hardware Requirements
Adafruit MacroPad RP2040  
Adafruit 1x4 NeoKey  
Quiic cable  

## Software Requirements
Arduino IDE  
Adafruit_NeoPixel library  
Adafruit_SH1106G library  
Adafruit_TinyUSB library  
Adafruit_USBD_CDC library  

## Installation
1. Install Arduino IDE and set it up for Adafruit MacroPad RP2040 as per the instructions here.
2. Install the necessary libraries (Adafruit_NeoPixel, Adafruit_SH1106G, Adafruit_TinyUSB, Adafruit_USBD_CDC) using the Library Manager in Arduino IDE.
3. Clone this repository to your local machine.
4. Open macropad.ino in Arduino IDE.
5. While holding the rotary key, connect your Adafruit MacroPad RP2040 to your computer, which enters the UF2 bootloader.
6. Select the correct board and port in Arduino IDE, and build the code.
7. Upload the built uf2 file to your Adafruit MacroPad RP2040.

## Usage
1. Physically connect the additional Adafruit 1x4 NeoKey to the Adafruit MacroPad RP2040 using the Quiic cable.
2. Secure both parts onto a surface using double-sided mounting tape, glue, or a 3D-printed case.
3. Power up the Adafruit MacroPad RP2040.
4. Press the rotary knob for settings, turn the knob to change pages.
5. Enjoy your Enhanced MacroPad!
